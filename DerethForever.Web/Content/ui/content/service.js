﻿
/// <reference path="../../../Scripts/vue.js" />

function makeContentService() {
    return new Vue({
        data() {
            return {
                patches: []
            };
        },
        methods: {
            fetch(id) {
                var $this = this;
                return new Promise((resolve, reject) => {
                    if (id === undefined) {
                        $.getJSON("/Patch/Patches",
                            function (res) {
                                $this.patches = res;
                                resolve(res);
                            })
                            .fail(() => reject());
                    } else {
                        $.getJSON("/Patch/Content", { patchId: id },
                            function (res) {
                                resolve(res);
                            })
                            .fail(() => reject());
                    }
                });
            },
            post(action, item) {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        type: "POST",
                        url: `/Patch/${action}`,
                        data: JSON.stringify(item),
                        contentType: 'application/json',
                        processData: false,
                        success: function (data, status, xhr) {
                            resolve(data);
                        },
                        error: function (xhr, status, err) {
                            reject(status);
                        }
                    });
                });
            },
            save(item) { return this.post('Save', item); },
            saveContent(item) { return this.post('SaveContent', item); },
            deleteContent(item) { return this.post('DeleteContent', item); },
            checkOutContent(item) { return this.post('CheckOutContent', item); },
            checkInContent(item) { return this.post('CheckInContent', item); }
        },
        created() {
            this.fetch();
        }
    });
}

const ContentStore = {
    install(Vue, options) {
        Vue.mixin({
            beforeCreate() {
                const opts = this.$options;
                if (opts.contentStore) {
                    this.$content = opts.contentStore;
                } else if (opts.parent && opts.parent.$content) {
                    this.$content = opts.parent.$content;
                }
            }
        });
    }
};
