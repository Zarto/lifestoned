﻿
/// <reference path="../../../Scripts/vue.js" />

function v2s(v) {
    return `${v.x.toFixed(6)} ${v.y.toFixed(6)} ${v.z.toFixed(6)}`;
}
function s2v(s) {
    if (!s) return null;
    var xyz = s.split(' ');
    if (xyz.length !== 3) return null;

    xyz[0] = parseFloat(xyz[0]);
    xyz[1] = parseFloat(xyz[1]);
    xyz[2] = parseFloat(xyz[2]);

    return { x: xyz[0], y: xyz[1], z: xyz[2] };
}

function q2s(q) {
    return `${q.w.toFixed(6)} ${q.x.toFixed(6)} ${q.y.toFixed(6)} ${q.z.toFixed(6)}`;
}
function s2q(s) {
    if (!s) return null;
    var wxyz = s.split(' ');
    if (wxyz.length !== 4) return null;

    wxyz[0] = parseFloat(wxyz[0]);
    wxyz[1] = parseFloat(wxyz[1]);
    wxyz[2] = parseFloat(wxyz[2]);
    wxyz[3] = parseFloat(wxyz[3]);

    return { w: wxyz[0], x: wxyz[1], y: wxyz[2], z: wxyz[3] };
}

function f2s(f) {
    return `[${v2s(f.origin)}] ${q2s(f.angles)}`;
}
function s2f(s) {
    if (!s) return null;

    var l = s.indexOf('[');
    var r = s.indexOf(']');
    var v = s.substring(l + 1, r);
    var q = s.substring(r + 2);

    return { origin: s2v(v), angles: s2q(q) };
}

function frame_zero() {
    return {
        origin: { x: 0.0, y: 0.0, z: 0.0 },
        angles: { w: 1.0, x: 0.0, y: 0.0, z: 0.0 }
    };
}

function pos_zero() {
    return {
        objcell_id: 0,
        frame: frame_zero()
    };
}

Vue.component('lsd-position', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    computed: {
        display: {
            get() { return this.data ? `0x${this.data.objcell_id.toHexStr()} ${f2s(this.data.frame)}` : null; },
            set(val) {
                if (!this.$el.validity.valid) return;

                var s = val.indexOf(' ');
                var lb = parseInt(val.substring(0, s), 16);
                var f = s2f(val.substring(s + 1));

                if (f) this.$emit('changed', { objcell_id: lb, frame: f });
            }
        }
    },
    methods: {
    },
    template: `
    <input v-model="display" type="text" class="form-control" pattern="0x[0-9A-Fa-f]{8} \\[(-?\\d+\\.\\d+\\s?){3}\\] (-?\\d+\\.\\d+\\s?){4}" />
    `
});

Vue.component('lsd-frame', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    computed: {
        display: {
            get() { return f2s(this.data ? this.data : frame_zero()); },
            set(val) {
                if (!this.$el.validity.valid) return;

                var f = s2f(val);
                if (f) this.$emit('changed', f);
            }
        }
    },
    methods: {
    },
    template: `
    <input v-model="display" type="text" class="form-control" pattern="\\[(-?\\d+\\.\\d+\\s?){3}\\] (-?\\d+\\.\\d+\\s?){4}" />
    `
});

Vue.component('lsd-vector', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    computed: {
        display: {
            get() { return this.data ? v2s(this.data) : null; },
            set(val) {
                if (!this.$el.validity.valid) return;

                var v = s2v(val);
                if (v) this.$emit('changed', v);
            }
        }
    },
    methods: {
    },
    template: `
    <input v-model="display" type="text" class="form-control" pattern="(-?\\d+\\.\\d+\\s?){3}" />
    `
});

Vue.component('lsd-quaternion', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    computed: {
        display: {
            get() { return this.data ? q2s(this.data) : null; },
            set(val) {
                if (!this.$el.validity.valid) return;

                var q = s2q(val);
                if (q) this.$emit('changed', q);
            }
        }
    },
    methods: {
    },
    template: `
    <input v-model="display" type="text" class="form-control" pattern="(-?\\d+\\.\\d+\\s?){4}" />
    `
});
