﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-table', {
    props: {
        title: { type: String, default: null },
        items: { type: Array, default: [] },
        showHeader: { type: Boolean, default: true }
    },
    data() {
        return {
            columns: []
        };
    },
    provide() {
        return {
            addCol: this.addCol
        };
    },
    methods: {
        addCol(comp) {
            console.log('adding', comp);

            let props = comp.componentOptions.propsData;
            let title = props.title;
            let key = props.bindKey;
            let readOnly = props.readOnly;
            let cssClass = comp.data.staticClass;
            let style = comp.data.staticStyle;
            let opts = comp.componentOptions.Ctor.options;
            let render = opts.template ? Vue.compile(opts.template).render : opts.render;

            this.columns.push({ title, key, readOnly, cssClass, style, comp, render });
        },

        createComp(h, comp, data) {
            let c = comp.comp;
            let ctor = c.componentOptions.Ctor;
            let props = c.componentOptions;
            props.propsData.item = data;
            let obj = new ctor(props);

            let slots = c.data.scopedSlots;
            if (slots) {
                //let o = comp.render.call(obj, h);
                let s = slots.default({ item: data });
                //console.log('create', o, s);
                return s;
            } else {
                return comp.render.call(obj, h);
            }
        },

        show() {
            $(this.$refs.container).modal('show');
        },

        hide() {
            $(this.$refs.container).modal('hide');
        },

        saved() {
            this.$emit('saved');
            this.hide();
        }
    },
    created() {
        for (let slot of this.$slots.default) {
            let s = slot;
            if (slot.tag && slot.componentOptions) {
                let opts = slot.componentOptions;
                if (opts.tag === 'lsd-table-col') {
                    this.addCol(s);
                }
            }
        }
    },
    mounted() {
        //console.log('table mounted', this);
    },
    updated() {
        //console.log('table updated', this);
    },
    render(h) {
        //console.log('table', this);

        let nodes = [];
        if (this.showHeader && this.columns.length) {
            let head = h('thead', [
                h('tr', this.columns.map((v, i, a) => h('th', { style: v.style }, v.title)))
            ]);
            nodes.push(head);
        }
        nodes.push(h('tfoot', 'footer'));

        let body = h('tbody',
            this.items.map(
                (vi, ii, ai) => h('tr', this.columns.map(
                    (vc, ic, ac) => h('td', { item: vi }, [ this.createComp(h, vc, vi) ] ))))
        );
        nodes.push(body);
        return h('table', { class: 'table table-striped table-condensed table-hover' }, nodes);
    }/*,
    template: `
    <table>
        <thead v-if="title || showHeader">
            <tr v-if="title"><caption>{{title}}</caption></tr>
            <tr v-if="showHeader && columns.length">
                <th v-for="(col, idx) in columns">{{ col.title }}</th>
            </tr>
        </thead>

        <tfoot>
            <tr><td>footer</td></tr>
        </tfoot>

        <tbody>
            <tr v-for="(row, idx) in items" :key="$hash(row)">
                <td v-for="col in columns">
                <component v-bind:is="createComp(col.comp)"></component>
                </td>
            </tr>
        </tbody>

    </table>
    `*/
});

Vue.component('lsd-table-col', {
    props: {
        title: { type: String, default: null },
        bindKey: { type: String },
        readOnly: { type: Boolean, default: false },
        item: { type: Object, default: { Key: -1 } }
    },
    created() {
        //console.log('lsd-table-col created', this);
    },
    mounted() {
        //console.log('lsd-table-col mounted', this);
    },
    updated() {
        //console.log('lsd-table-col updated', this);
    },
    destroyed() {
    },
    template: `
    <span>
    <slot :item="item"><span>{{item[bindKey]}}</span></slot>
    </span>
    `
});
