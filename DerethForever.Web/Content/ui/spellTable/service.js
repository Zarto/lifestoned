﻿
/// <reference path="../../../Scripts/vue.js" />

function makeSpellTableService() {
    return new Vue({
        data() {
            return {
                table: []
            };
        },
        watch: {
        },
        methods: {
            search(name, school, category) {
                var $this = this;
                $.getJSON("/SpellTable/Search", { name, school, category },
                    function (res) {
                        $this.table = res;
                    });
            },
            fetch(id) {
                return new Promise((resolve, reject) => {
                    $.getJSON("/SpellTable/Get", { id: id },
                        function (res) {
                            resolve(res);
                        })
                        .fail(() => reject());
                });
            },
            save(spell) {
                if (spell.key !== spell.value.meta_spell.spell.spell_id) {
                    spell.value.meta_spell.spell.spell_id = spell.key;
                }
                if (spell.value.category !== spell.value.meta_spell.spell.spellCategory) {
                    spell.value.meta_spell.spell.spellCategory = spell.value.category;
                }

                $.ajax({
                    type: "PUT",
                    url: "/SpellTable/Put",
                    data: JSON.stringify(spell),
                    contentType: 'application/json',
                    processData: false,
                    success: function (data, status, xhr) {
                        alert('Save Successful');
                        console.log(data, status);
                    },
                    error: function (xhr, status, err) {
                        alert('Save Failed');
                        console.error(status, err);
                    }
                });
            },
            newSpell() {
                return {
                    key: null,
                    lastModified: null,
                    modifiedBy: null,
                    userChangeSummary: null,
                    isDone: null,
                    changelog: [],
                    value: {
                        base_mana: null,
                        base_range_constant: null,
                        base_range_mod: null,
                        bitfield: null,
                        caster_effect: null,
                        category: null,
                        component_loss: null,
                        desc: null,
                        display_order: null,
                        fizzle_effect: null,
                        formula: [],
                        formula_version: null,
                        iconID: null,
                        mana_mod: null,
                        name: null,
                        non_component_target_type: null,
                        power: null,
                        recovery_amount: null,
                        recovery_interval: null,
                        school: null,
                        spell_economy_mod: null,
                        target_effect: null,
                        meta_spell: {
                            sp_type: null,
                            spell: {
                                degrade_limit: null,
                                degrade_modifier: null,
                                duration: null,
                                spellCategory: null,
                                spell_id: null,
                                smod: {
                                    key: null,
                                    type: null,
                                    val: null
                                }

                            }

                        }
                    }
                };
            }
        },
        created() {
        }
    });
}

const SpellTableStore = {
    install(Vue, options) {
        Vue.mixin({
            beforeCreate() {
                const opts = this.$options;
                if (opts.spellTableStore) {
                    this.$spellTable = opts.spellTableStore;
                } else if (opts.parent && opts.parent.$spellTable) {
                    this.$spellTable = opts.parent.$spellTable;
                }
            }
        });
    }
};
