jQuery(document).ready(function(){
	 $("#btnViewAppraise").on('click', function(e){
		 var appraise = new ItemExamineUI();
         appraise.SetName();
         appraise.SetAppraiseInfo();
         $("#appraise .modal-title").html(appraise.name);
         $("#appraise .modal-body").html(appraise.getDisplayText());
         $("#appraise").modal("show");
	 });
});

var ItemExamineUI = function () {
    this.m_itemDisplayText = "";
    this.name = "";
}
ItemExamineUI.prototype.SetName = function () {
    this.name = "";
    var name = this.InqString(1);
    if (name !== false) {
        var iMaterialType = this.InqInt(131);
        if (iMaterialType !== false) {
            var strMaterial = this.InqMaterialName(iMaterialType);
            this.name = strMaterial + ' ' + name;
        } else {
            this.name = name;
        }
    }
}

ItemExamineUI.prototype.getDisplayText = function () {
    // cleans up the extra lines, which isn't client accurate, but looks better. Returns with HTML breaks.
    var text = this.m_itemDisplayText.replace(/\n\s*\n\s*\n/g, '\n\n');
    if (text.charAt(0) == "\n") text = text.slice(1); //remove leading \n
    text += "\n\n"; //gives a little padding in the body
    text = text.replace(/\n/g, "<br>");
    return text;
}
// NOTE "font" is used in the client to contorl color. We're not going to bother since there's no buffed/debuffed stats.
ItemExamineUI.prototype.AddItemInfo = function (text, font, surpressDoubleSpacing) {
    if (surpressDoubleSpacing == 1) {
        this.m_itemDisplayText += "\n" + text; // \n in client. Swapped for HTML break tag instead.
    } else {
        this.m_itemDisplayText += "\n\n" + text; // \n\n in client. Swapped for HTML break tag instead.
    }
}

ItemExamineUI.prototype.IsCreature = function () {
    var itemType = this.InqInt(1);
    var npcLikeObject = this.InqBool(83);
    if (itemType == 16 && npcLikeObject != true) {
        return true;
    }
    return false;
}

ItemExamineUI.prototype.SetAppraiseInfo = function () {
    if (this.IsCreature()) {
        this.SetCreatureInfo();
    } else {
        this.m_itemDisplayText = "";

        this.SetValueText();
        this.Appraisal_ShowBurdenInfo();
        this.Appraisal_ShowTinkeringInfo();
        var hasSet = this.Appraisal_ShowSet();
        var numRatings = this.Appraisal_ShowRatings();
        if (hasSet == true || numRatings > 0) {
            this.AddItemInfo("", 0, 1);
        }
        this.Appraisal_ShowWeaponAndArmorData();
        this.Appraisal_ShowDefenseModData();
        this.Appraisal_ShowArmorMods();
        this.Appraisal_ShowShortMagicInfo();
        this.Appraisal_ShowSpecialProperties();
        this.Appraisal_ShowUsage();
        this.Appraisal_ShowLevelLimitInfo();
        this.Appraisal_ShowWieldRequirements();
        this.Appraisal_ShowUsageLimitInfo();
        this.Appraisal_ShowItemLevelInfo();
        this.Appraisal_ShowActivationRequirements();
        this.Appraisal_ShowCasterData();
        this.Appraisal_ShowBoostValue();
        this.Appraisal_ShowHealKitValues();
        this.Appraisal_ShowCapacity();
        this.Appraisal_ShowLockAppraiseInfo();
        this.Appraisal_ShowManaStoneInfo();
        this.Appraisal_ShowRemainingUses();
        this.Appraisal_ShowCraftsman();

        var unsellable = this.InqBool(0x45);
        if (unsellable === 0)
            this.AddItemInfo("This item cannot be sold.", 0, 1);

        this.Appraisal_ShowRareInfo();
        this.Appraisal_ShowMagicInfo();
        this.Appraisal_ShowDescription();
    }
}

ItemExamineUI.prototype.SetCreatureInfo = function () {
    this.m_itemDisplayText = "";
    var level = this.InqInt(25);
    if (level != false) {
        this.AddItemInfo("Character Level: " + level, 0, 1);
    } else {
        this.AddItemInfo("Character Level: ???", 0, 1);
    }
    this.AddItemInfo("", 0, 1);

    var ranks = [];
    var atts = ["strength", "endurance", "coordination", "quickness", "focus", "self"];
    var attNames = ["Strength", "Endurance", "Coordination", "Quickness", "Focus", "Self"];
    for (var i = 0; i < atts.length; i++) {
        var attribute = this.InqAttribute(atts[i]);
        if (attribute != false) {
            this.AddItemInfo(attNames[i] + ": " + attribute.init_level, 0, 1);
            ranks[atts[i]] = attribute.init_level;
        } else {
            this.AddItemInfo(attNames[i] + ": ???", 0, 1);
            ranks[atts[i]] = 0;
        }
    }
    this.AddItemInfo("", 0, 1);

    var health = this.InqAttribute("health");
    if (health != false) {
        var base = health.init_level;
        var val = base + parseInt(ranks.endurance / 2);
        this.AddItemInfo("Health: " + health.current + " / " + val, 0, 1);
    } else {
        this.AddItemInfo("Health: ???", 0, 1);
    }
    var stamina = this.InqAttribute("stamina");
    if (stamina != false) {
        var base = stamina.init_level;
        var val = base + ranks.endurance;
        this.AddItemInfo("Stamina: " + stamina.current + " / " + val, 0, 1);
    } else {
        this.AddItemInfo("Stamina: ???", 0, 1);
    }
    var mana = this.InqAttribute("mana");
    if (mana != false) {
        var base = mana.init_level;
        var val = base + ranks.self;
        this.AddItemInfo("Mana: " + mana.current + " / " + val, 0, 1);
    } else {
        this.AddItemInfo("Mana: ???", 0, 1);
    }
}

ItemExamineUI.prototype.SetValueText = function () {
    var iValue = this.InqInt(0x13);
    if (iValue !== false) {
        this.AddItemInfo("Value: " + iValue.toLocaleString(), 0, 1);
    } else {
        this.AddItemInfo("Value: ???", 0, 1);
    }
}
ItemExamineUI.prototype.Appraisal_ShowBurdenInfo = function () {
    var iValue = this.InqInt(5);
    if (iValue !== false) {
        this.AddItemInfo("Burden: " + iValue.toLocaleString(), 0, 1);
    } else {
        this.AddItemInfo("Burden: Unknown", 0, 1);
    }
    this.AddItemInfo("", 0, 1);
}

ItemExamineUI.prototype.Appraisal_ShowTinkeringInfo = function () {
    var cTinkers = this.InqInt(0xAB);
    if (cTinkers !== false && cTinkers > 0) {
        if (cTinkers <= 1)
            var tinkerPlural = '';
        else
            var tinkerPlural = 's';
        var tinkerText = "This item has been tinkered {0} time{1}.".format(cTinkers, tinkerPlural);
        this.AddItemInfo(tinkerText, 0, 1);
    }

    var strTinkerer = this.InqString(0x27);
    if (strTinkerer !== false) {
        var text = "Last tinkered by {0}.".format(strTinkerer);
        this.AddItemInfo(text, 0, 1);
    }

    var strImbuer = this.InqString(0x28);
    if (strImbuer !== false) {
        var text = "Imbued by {0}.".format(strImbuer);
        this.AddItemInfo(text, 0, 1);
    }

    var iWorkmanship = this.InqInt(0x69);
    if (iWorkmanship !== false) {
        var iNumItems = this.InqInt(0xAA);
        if (iNumItems !== false) {
            var fAvg = iWorkmanship / iNumItems;
            var strWorkmanship = this.InqWorkmanshipAdjective(iWorkmanship, 0);
            var text = "Workmanship: {0} ({1})\n\nSalvaged from {2} items.".format(strWorkmanship, parseFloat(fAvg.toFixed(2)), iNumItems);
        } else {
            var strWorkmanship = this.InqWorkmanshipAdjective(iWorkmanship, 0);
            var text = "Workmanship: {0} ({1})".format(strWorkmanship, iWorkmanship);
        }
        this.AddItemInfo(text, 0, 1);
    }
}
ItemExamineUI.prototype.Appraisal_ShowSet = function () {
    var setBonus = this.InqInt(0x109);
    if (setBonus != false) {
        var prof = this.GetArmorSet(setBonus);
        var text = "Set: " + prof;
        this.AddItemInfo(text, 0, 1);
        return true;
    } else {
        return false;
    }
}
ItemExamineUI.prototype.Appraisal_ShowRatings = function () {
    var damage_rating = this.InqInt(0x172);
    var damage_resist_rating = this.InqInt(0x173);
    var crit_rating = this.InqInt(0x174);
    var crit_damage_rating = this.InqInt(0x176);
    var crit_resist_rating = this.InqInt(0x175);
    var crit_damage_resist_rating = this.InqInt(0x177);
    var healing_boost_rating = this.InqInt(0x178);
    var life_resist_rating = this.InqInt(0x17A);
    var nether_resist_rating = this.InqInt(0x179);
    var gear_max_health = this.InqInt(0x17B);

    // In client 6096
    var overpower = this.InqInt(386); //v97 - 388?
    var overpower_reduction = this.InqInt(387); //v98 - 389?
    var pk_dam = this.InqInt(381); // v99 - 383?
    var pm_dam_resist = this.InqInt(382); //v100 - 384?

    var text = "";
    if (damage_rating > 0) {
        text += "Dam {0}, ".format(damage_rating);
    }
    if (damage_resist_rating > 0) {
        text += "Dam Resist {0}, ".format(damage_resist_rating);
    }
    if (crit_rating > 0) {
        text += "Crit {0}, ".format(crit_rating);
    }
    if (crit_damage_rating > 0) {
        text += "Crit Dam {0}, ".format(crit_damage_rating);
    }
    if (crit_resist_rating > 0) {
        text += "Crit Resist {0}, ".format(crit_resist_rating);
    }
    if (crit_damage_resist_rating > 0) {
        text += "Crit Dam Resist {0}, ".format(crit_damage_resist_rating);
    }
    if (overpower > 0) {
        text += "Overpower% {0}, ".format(overpower);
    }
    if (overpower_reduction > 0) {
        text += "Overpower Reduction% {0}, ".format(overpower_reduction);
    }
    if (pk_dam > 0) {
        text += "PK Dam {0}, ".format(pk_dam);
    }
    if (pm_dam_resist > 0) {
        text += "PK Dam Resist {0}, ".format(pm_dam_resist);
    }
    if (healing_boost_rating > 0) {
        text += "Heal Boost {0}, ".format(healing_boost_rating);
    }
    if (nether_resist_rating > 0) {
        text += "Nether Resist {0}, ".format(nether_resist_rating);
    }
    if (life_resist_rating > 0) {
        text += "Life Resist {0}, ".format(life_resist_rating);
    }

    text = text.trim();
    if (text.charAt(text.length - 1) == ",") text = text.slice(0, -1); //remove any trailing commas
    if (text != "") {
        text = "Ratings: " + text + ".";
        this.AddItemInfo(text, 0, 1);
    }

    if (gear_max_health > 0) {
        var gearText = "This item adds {0} Vitality.".format(gear_max_health);
        this.AddItemInfo(gearText, 0, 1);
    }

    if (text != "" || gear_max_health > 0) {
        return 1;
    } else {
        return 0;
    }
}
ItemExamineUI.prototype.Appraisal_ShowWeaponAndArmorData = function () {
    var text = "";
    var ammoType = this.InqInt(50);
    if (ammoType !== false) {
        // this.GetHookedItemAmmoType();
    }
    var raised;
    var lm = this.InqInt(9);// _valid_locations;
    if ((lm & 0x3F00000) !== 0) {
        if ((lm & 0x200000) !== 0) {
            var retval = this.InqInt(0x1C);
            if (retval !== false) {
                this.AddItemInfo("Base Shield Level: {0}".format(retval), 0, 1);
            } else {
                this.AddItemInfo("Shield Level: Unknown", 0, 1);
            }
        }

        var itemType = this.InqInt(1);
        //if is a weapon
        if (itemType !== false && (itemType == 1 || itemType == 256 || itemType == 257 || itemType == 32768 || itemType == 0x8101)) {
            var ammoText;
            if ((lm & 0x400000) !== 0) {
                if (ammoType == 1) {
                    ammoText = "Uses arrows as ammunition.";
                } else if (ammoType == 2) {
                    ammoText = "Uses quarrels as ammunition.";
                } else if (ammoType == 4) {
                    ammoText = "Uses atlatl darts as ammunition.";
                }
            } else {
                if (ammoType == 1) {
                    ammoText = "Used as ammunition by bows.";
                } else if (ammoType == 2) {
                    ammoText = "Used as ammunition by crossbows.";
                } else if (ammoType == 4) {
                    ammoText = "Used as ammunition by atlatls.";
                }
            }

            var ps = "Skill: ";
            var weaponSkillInt = this.InqInt(48);
            if (weaponSkillInt !== false) {
                var SkillString = this.getSkill(weaponSkillInt);
                if (SkillString !== false)
                    ps += SkillString;

                var subWeaponType = this.InqInt(0x161);
                var rhs;
                switch (subWeaponType) {
                    case 1: rhs = " (Unarmed Weapon)"; break;
                    case 2: rhs = " (Sword)"; break;
                    case 3: rhs = " (Axe)"; break;
                    case 4: rhs = " (Mace)"; break;
                    case 5: rhs = " (Spear)"; break;
                    case 6: rhs = " (Dagger)"; break;
                    case 7: rhs = " (Staff)"; break;
                    case 8: rhs = " (Bow)"; break;
                    case 9: rhs = " (Crossbow)"; break;
                    case 10: rhs = " (Thrown)"; break;
                    default: rhs = "";
                }
                this.AddItemInfo(ps + rhs, 0, 1);
            }

            var raised = lm & 0x400000 && ammoType;
            // NOT SURE WHAT THIS IS...
            if (raised > 0) {
                var damageTxt = "Damage Bonus: ";
            } else {
                var damageTxt = "Damage: ";
            }

            var weaponDamage = this.InqInt(44);
            var _damageType = this.InqInt(45);
            if (weaponDamage !== false) {
                if (weaponDamage !== false && weaponDamage <= -1) {
                    var ability_txt = "{0}{1}".format(damageTxt, "Unknown");
                    this.AddItemInfo(ability_txt, 0, 1);
                } else {
                    var Dest = "";
                    if (!raised) {
                        Dest = ", unknown type";
                        if (weaponDamage !== false) {
                            var buf = this.DamageTypeToString(_damageType);
                            Dest = ", {0}".format(buf);
                        }
                    }
                    var damage_variance = this.InqFloat(22);
                    var rhs = (1.0 - damage_variance) * weaponDamage;
                    if ((weaponDamage - rhs) > 0.00019999999) {
                        if (rhs >= 10.0)
                            var ability_txt = "{0}{1} - {2}{3}".format(damageTxt, parseFloat(rhs.toFixed(4)), weaponDamage, Dest);
                        else
                            var ability_txt = "{0}{1} - {2}{3}".format(damageTxt, parseFloat(rhs.toFixed(3)), weaponDamage, Dest);
                    } else {
                        var ability_txt = "{0}{1}{2}".format(damageTxt, weaponDamage, Dest);
                    }
                }
                this.AddItemInfo(ability_txt, 0, 1);
            }
        }

        var retval = this.InqInt(0xCC);
        if (retval > 0 && retval !== false) {
            var _damageType = this.InqInt(45);
            var buf = this.DamageTypeToString(_damageType);
            var rhs = "Elemental Damage Bonus: {0}, {1}.".format(retval, buf);
            this.AddItemInfo(rhs, 0, 1);
        }
        v9 = (lm & 0x400000);
        if (v9 && ammoType) {
            if (typeof subWeaponType != "undefined") {
                var v15 = this.ModifierToString(this.InqFloat(63)); // Damage Mod
                var ability_txt = "Damage Modifier: {0}.".format(v15);
                this.AddItemInfo(ability_txt, 0, 1);
            }
        }
    }

    if ((lm & 0x2500000) > 0) {
        var wap_weapon_time = this.InqInt(49);
        if (wap_weapon_time <= -1 || wap_weapon_time === false) {
            this.AddItemInfo("Speed:  Unknown", 0, 1);
            if (v9 > 0) {
                this.AddItemInfo("Range:  Unknown", 0, 1);
            }
        } else {
            var ps = "Speed: ";
            ps += this.WeaponTimeToString(wap_weapon_time);
            var ability_txt = "{0} ({1})".format(ps, wap_weapon_time);
            this.AddItemInfo(ability_txt, 0, 1);
            if (v9) {
                var wap_max_velocity = this.InqFloat(26);
                ps = Math.pow(wap_max_velocity, 2.0) * 0.1020408163265306 * 1.094;
                var MISSILE_RANGE_CAP = 85.0;
                if (ps > MISSILE_RANGE_CAP)
                    ps = MISSILE_RANGE_CAP;
                if (ps >= 10) {
                    var v19 = ps - parseInt(ps % 5);
                } else {
                    v19 = ps;
                }
                ability_txt = "Range: {0} yds.".format(parseInt(v19));
                this.AddItemInfo(ability_txt, 0, 1);
            }
        }
    }
    var wap_weapon_offense = this.InqFloat(62);
    if (wap_weapon_offense != 1 && wap_weapon_offense !== false && !raised) {
        ability_txt = "Bonus to Attack Skill: {0}%.".format(this.SmallModifierToString(wap_weapon_offense));
        this.AddItemInfo(ability_txt, 0, 1);
    }
    if (typeof ammoText != "undefined") {
        this.AddItemInfo(ammoText, 0, 1);
    }

    var priority = this.InqInt(4); // CLOTHING_PRIORITY_INT
    var ps = this.ClothingPriorityToString(priority);
    if (priority !== false && ps != false && ((lm & 0x8007FFF) > 0)) {
        this.AddItemInfo(ps, 0, 1);
    }
}
ItemExamineUI.prototype.Appraisal_ShowDefenseModData = function () {
    var rDefenseModifier = this.InqFloat(0x1D);
    if (rDefenseModifier !== false && rDefenseModifier != 1) {
        var szBuff = "Bonus to Melee Defense: {0}%.".format(this.SmallModifierToString(rDefenseModifier));
        this.AddItemInfo(szBuff, 0, 1);
    }

    rDefenseModifier = this.InqFloat(0x95);
    if (rDefenseModifier !== false && rDefenseModifier != 1) {
        szBuff = "Bonus to Missile Defense: {0}%.".format(this.SmallModifierToString(rDefenseModifier));
        this.AddItemInfo(szBuff, 0, 1);
    }

    rDefenseModifier = this.InqFloat(0x96);
    if (rDefenseModifier !== false && rDefenseModifier != 1) {
        szBuff = "Bonus to Magic Defense: {0}%.".format(this.SmallModifierToString(rDefenseModifier));
        this.AddItemInfo(szBuff, 0, 1);
    }
}
ItemExamineUI.prototype.Appraisal_ShowArmorMods = function () {
    var aap = this.InqArmor();
    var armor_lvl = this.InqInt(0x1C); // ARMOR_LEVEL_INT 
    if (armor_lvl > 0) {
        this.AddItemInfo("<br>Armor Level: " + armor_lvl, 0, 1);

        this.AddItemInfo(this.DamageResistanceToString(1, armor_lvl, aap.mod_vs_slash), 0, 1);
        this.AddItemInfo(this.DamageResistanceToString(2, armor_lvl, aap.mod_vs_pierce), 0, 1);
        this.AddItemInfo(this.DamageResistanceToString(4, armor_lvl, aap.mod_vs_bludgeon), 0, 1);
        this.AddItemInfo(this.DamageResistanceToString(16, armor_lvl, aap.mod_vs_fire), 0, 1);
        this.AddItemInfo(this.DamageResistanceToString(8, armor_lvl, aap.mod_vs_cold), 0, 1);
        this.AddItemInfo(this.DamageResistanceToString(32, armor_lvl, aap.mod_vs_acid), 0, 1);
        this.AddItemInfo(this.DamageResistanceToString(64, armor_lvl, aap.mod_vs_electric), 0, 1);
        this.AddItemInfo(this.DamageResistanceToString(1024, armor_lvl, aap.mod_vs_nether), 0, 1);
    }
}
ItemExamineUI.prototype.InqArmor = function () {
    var aap = new Object();
    aap.mod_vs_slash = this.InqFloat(13);
    aap.mod_vs_pierce = this.InqFloat(14);
    aap.mod_vs_bludgeon = this.InqFloat(15);
    aap.mod_vs_cold = this.InqFloat(16);
    aap.mod_vs_fire = this.InqFloat(17);
    aap.mod_vs_acid = this.InqFloat(18);
    aap.mod_vs_electric = this.InqFloat(19);
    aap.mod_vs_nether = this.InqFloat(165);
    return aap;
}

ItemExamineUI.prototype.Appraisal_ShowShortMagicInfo = function () {
    var spells = this.InqSpells();
    var spellEnum = JSON.parse(localStorage.getItem("spells_list"));
    //console.log({ spellEnum });
    if (spells != false && spells.length > 0) {
        var spellTxt = "Spells: ";
        var spellNamesTxt = "";
        for (var i = 0; i < spells.length; i++) {
            if ((spells[i] & 0x80000000) == 0) { // skips the cooldown enchantments. Not really applicable but might as well check
                var strSpell = this.InqSpellEnum(spellEnum, spells[i]);
                spellNamesTxt += strSpell + ', ';
                console.log({ strSpell });
            }
        }
        spellNamesTxt = spellNamesTxt.trim();
        if (spellNamesTxt.charAt(spellNamesTxt.length - 1) == ",") spellNamesTxt = spellNamesTxt.slice(0, -1); //remove any trailing commas

        if (spellNamesTxt != "") {
            this.AddItemInfo(spellTxt + spellNamesTxt, 0, 0);
        }
    }
}

ItemExamineUI.prototype.Appraisal_ShowSpecialProperties = function () {
    this.AddItemInfo("", 0, 1);
    var infoAddedToPanel = false;
    var iUnique = this.InqInt(0x117);
    if (iUnique !== false) {
        this.AddItemInfo("You can only carry {0} of these items.".format(iUnique), 0, 0);
    }
    var iCooldown = this.InqFloat(0xA7); // COOLDOWN_DURATION_FLOAT 
    if (iCooldown !== false) {
        var sTextStr = "Cooldown When Used: " + this.DeltaTimeToString(iCooldown);;
        this.AddItemInfo(sTextStr, 0, 0);
    }
    // sharedCooldown "Cooldown Remaining: "

    this.AddItemInfo("", 0, 1);
    var strProperties = "";
    var cleaving = this.InqInt(0x124); // CLEAVING_INT
    if (cleaving > 0) {
        this.AddItemInfo("Cleave: {0} enemies in front arc.".format(cleaving), 0, 1);
        this.AddItemInfo("", 0, 1);
    }

    var creatureType = this.InqInt(0xA6);
    if (creatureType > 0) {
        var creatureTxt;
        if (creatureType == 31) { // Special Case
            creatureTxt = "Bael'Zharon's Hate";
        } else {
            var creatureName = this.InqCreatureDisplayName(creatureType);
            creatureTxt = creatureName + " slayer";
        }
        strProperties = this.AppendHelper(strProperties, creatureTxt);
    }

    var attacktype = this.InqInt(0x2F);
    if ((attacktype & 0x79E0) > 0) {
        strProperties = this.AppendHelper(strProperties, "Multi-Strike");
    }

    var imbued = false;
    if (this.InqInt(0xB3) !== false) imbued |= this.InqInt(0xB3);
    if (this.InqInt(0x12F) !== false) imbued |= this.InqInt(0xB3);
    if (this.InqInt(0x130) !== false) imbued |= this.InqInt(0xB3);
    if (this.InqInt(0x131) !== false) imbued |= this.InqInt(0xB3);
    if (this.InqInt(0x132) !== false) imbued |= this.InqInt(0xB3);
    if (imbued) {
        if ((imbued & 1) > 0) strProperties = this.AppendHelper(strProperties, "Critical Strike");
        if ((imbued & 2) > 0) strProperties = this.AppendHelper(strProperties, "Crippling Blow");
        if ((imbued & 4) > 0) strProperties = this.AppendHelper(strProperties, "Armor Rending");
        if ((imbued & 8) > 0) strProperties = this.AppendHelper(strProperties, "Slash Rending");
        if ((imbued & 0x10) > 0) strProperties = this.AppendHelper(strProperties, "Pierce Rending");
        if ((imbued & 0x20) > 0) strProperties = this.AppendHelper(strProperties, "Bludgeon Rending");
        if ((imbued & 0x40) > 0) strProperties = this.AppendHelper(strProperties, "Acid Rending");
        if ((imbued & 0x4000) > 0) strProperties = this.AppendHelper(strProperties, "Nether Rending");
        if ((imbued & 0x80) > 0) strProperties = this.AppendHelper(strProperties, "Cold Rending");
        if ((imbued & 0x100) > 0) strProperties = this.AppendHelper(strProperties, "Lightning Rending");
        if ((imbued & 0x200) > 0) strProperties = this.AppendHelper(strProperties, "Fire Rending");
        if ((imbued & 0x400) > 0) strProperties = this.AppendHelper(strProperties, "+1 Melee Defense");
        if ((imbued & 0x800) > 0) strProperties = this.AppendHelper(strProperties, "+1 Missile Defense");
        if ((imbued & 0x1000) > 0) strProperties = this.AppendHelper(strProperties, "+1 Magic Defense");
        if (imbued < 0) strProperties = this.AppendHelper(strProperties, "Phantasmal");
    }

    var absorb_magic = this.InqFloat(0x9F);
    if (absorb_magic > 0) strProperties = this.AppendHelper(strProperties, "Magic Absorbing");

    var magic_resist = this.InqInt(36);
    if (magic_resist >= 9999) strProperties = this.AppendHelper(strProperties, "Unenchantable");

    var attuned = this.InqInt(0x72);
    if (attuned == 1) {
        strProperties = this.AppendHelper(strProperties, "Attuned");
    }

    var bonded = this.InqInt(0x21);
    switch (bonded) {
        case -2: strProperties = this.AppendHelper(strProperties, "Destroyed on Death"); break;
        case -1: strProperties = this.AppendHelper(strProperties, "Dropped on Death"); break;
        case 1: strProperties = this.AppendHelper(strProperties, "Bonded"); break;
    }

    var retained = this.InqBool(0x5B);
    if (retained == 1) strProperties = this.AppendHelper(strProperties, "Retained");

    var fCrit = this.InqFloat(0x88);
    if (fCrit !== false) strProperties = this.AppendHelper(strProperties, "Crushing Blow");

    var fCrit = this.InqFloat(0x93);
    if (fCrit !== false) strProperties = this.AppendHelper(strProperties, "Biting Strike");

    var fCrit = this.InqFloat(0x9B);
    if (fCrit !== false) strProperties = this.AppendHelper(strProperties, "Armor Cleaving");

    var fResMod = this.InqFloat(0x9D);
    var damageType = this.InqInt(0x107);
    if (fResMod !== false && damageType !== false) {
        tmp_txt = this.DamageTypeToString(damageType);
        strProperties = this.AppendHelper(strProperties, "Crushing Blow");
        strProperties = this.AppendHelper(strProperties, "Resistance Cleaving: {0}".format(tmp_txt));
    }

    var spellDID = this.InqDataId(0x37);
    if (spellDID !== false) strProperties = this.AppendHelper(strProperties, "Cast on Strike");

    var bIvoryable = this.InqBool(0x63);
    if (bIvoryable == 1) strProperties = this.AppendHelper(strProperties, "Ivoryable");

    var bDyable = this.InqBool(0x64);
    if (bDyable == 1) strProperties = this.AppendHelper(strProperties, "Dyeable");

    if (strProperties != "")
        this.AddItemInfo('Properties: ' + strProperties, 0, 1);

    if (imbued) this.AddItemInfo("This item cannot be further imbued.", 0, 1);

    var bAutowieldLeft = this.InqBool(0x82);
    if (bAutowieldLeft == 1) this.AddItemInfo("This item is tethered to the left side.", 0, 1);
    this.AddItemInfo("", 0, 1);
}
ItemExamineUI.prototype.AppendHelper = function (src, textToAdd) {
    if (src != "") {
        src = src + ', ' + textToAdd;
        return src;
    }
    src = textToAdd;
    return src;
}
ItemExamineUI.prototype.Appraisal_ShowUsage = function () {
    var strUsage = this.InqString(0xE);
    if (strUsage !== false)
        this.AddItemInfo(strUsage, 0, 0);
}
ItemExamineUI.prototype.Appraisal_ShowLevelLimitInfo = function () {
    var min = this.InqInt(0x56);
    var max = this.InqInt(0x57);
    if (min !== false || max !== false) {
        var numtxt;
        if (min <= 0 && max > 0) {
            numtxt = "Restricted to characters of Level {0} or below.".format(max);
        } else if (max <= 0) {
            numtxt = "Restricted to characters of Level {0} or greater.".format(min);
        } else if (min == max) {
            numtxt = "Restricted to characters of Level {0}.".format(min);
        } else {
            numtxt = "Restricted to characters of Levels {0} to {1}.".format(min, max);
        }
        this.AddItemInfo(numtxt, 0, 0);
    }

    var portal_dest = this.InqString(0x26);
    if (portal_dest !== false) {
        this.AddItemInfo("Destination: " + portal_dest, 0, 0);
    }
}

ItemExamineUI.prototype.Appraisal_ShowWieldRequirements = function () {
    var has_allowed_wielder = this.InqBool(0x55); // APPRAISAL_HAS_ALLOWED_WIELDER_BOOL 
    if (has_allowed_wielder == 1) {
        var strWielder = "the original owner";
    }

    var iAcctReqs = this.InqInt(0x1A); //ACCOUNT_REQUIREMENTS_INT 
    if (iAcctReqs == 1) this.AddItemInfo("Use requires Throne of Destiny.", 0, 1);

    var heritage_specific = this.InqInt(0x144); // HERITAGE_SPECIFIC_ARMOR_INT 
    if (heritage_specific !== false) {
        heritageName = this.InqHeritageGroupDisplayName(heritage_specific);
        if (heritageName !== false) {
            this.AddItemInfo("Wield requires " + heritageName, 0, 1);
        }
    }

    var reqSets = [];
    reqSets.push({ "iReq": 0x9E, "iSkill": 0x9F, "iDiff": 0xA0 });
    reqSets.push({ "iReq": 0x10E, "iSkill": 0x10F, "iDiff": 0x110 });
    reqSets.push({ "iReq": 0x111, "iSkill": 0x112, "iDiff": 0x113 });
    reqSets.push({ "iReq": 0x114, "iSkill": 0x115, "iDiff": 0x116 });
    for (var i = 0; i < reqSets.length; i++) {
        var r = reqSets[i];
        var iReq = this.InqInt(r.iReq);
        var iSkill = this.InqInt(r.iSkill);
        var iDiff = this.InqInt(r.iDiff);
        if (iReq && iSkill && iDiff) {
            var strSkill = this.GetAppraisalStringFromRequirements(iReq, iSkill, iDiff);
            switch (iReq) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 9:
                case 10:
                    var numtxt = "Wield requires {0} {1}".format(strSkill, iDiff);
                    break;
                case 8:
                    var v13 = "specialized";
                    if (iDiff != 3)
                        v13 = "trained";
                    var numtxt = "Wield requires {0} {1}".format(v13, strSkill);
                    break;
                case 11: var numtxt = "Wield requires {0} type".format(strSkill); break;
                case 12: var numText = "Wield requires {0} race".format(strSkill); break;
                default: var numtxt = "";
            }
            if (numtxt !== "") this.AddItemInfo(numtxt, 0, 1);
        }
    }
}

ItemExamineUI.prototype.Appraisal_ShowUsageLimitInfo = function () {
    var level_requirement = this.InqInt(0x171);
    var requirementHandled = false;
    if (level_requirement > 0) {
        this.AddItemInfo("", 0, 1);
        this.AddItemInfo("Use requires level {0}.", format(level_requirement), 0, 1);
        requirementHandled = true;
    }

    var skill = this.InqInt(0x16E);
    var iskill_level = this.InqInt(0x16F);
    if (skill !== false && iskill_level !== false) {
        var skill_str = this.InqSkillName(skill);
        if (skill_str === false) skill_str = "Unknown Skill";
        if (!requirementHandled) this.AddItemInfo("", 0, 1);
        this.AddItemInfo("Use requires {0} of at least {1}.".format(skill_str, iskill_level), 0, 1);
        requirementHandled = true;
    }

    var skill = this.InqInt(0x170);
    if (skill !== false) {
        var skill_str = this.InqSkillName(skill);
        if (skill_str === false) skill_str = "Unknown Skill";
        if (!requirementHandled) this.AddItemInfo("", 0, 1);
        this.AddItemInfo("Use requires specialized {0}.".format(skill_str), 0, 1);
    }

}

ItemExamineUI.prototype.Appraisal_ShowItemLevelInfo = function () {
    var item_base_xp = this.InqInt64(5);
    var item_max_level = this.InqInt(0x13F);
    var item_xp_style = this.InqInt(0x140);

    // TODO - FIX THIS!
    if (item_base_xp > 0 && item_max_level > 0 && item_xp_style > 0) {
        var item_xp = this.InqInt64(4);
        var v5 = this.ItemTotalXPToLevel(item_xp, item_base_xp, item_max_level, item_xp_style);
        var v7 = v5 + 1;
        if (v7 > item_max_level) v7 = item_max_level;
        var v8 = this.ItemLevelToTotalXP(v7, item_base_xp, item_max_level, item_xp_style);
        var formatted_item_xp = item_xp.toLocaleString();
        var formatted_next_xp = v8.toLocaleString();

        var text = "Item Level: {0} / {1}".format(v5, item_max_level);
        this.AddItemInfo(text, 0, 1);
        text = "Item XP: {0} / {1}".format(formatted_item_xp, formatted_next_xp);
        this.AddItemInfo(text, 0, 1);
    }

    var cloak_proc = this.InqInt(0x160);
    if (cloak_proc == 2) {
        this.AddItemInfo("This cloak has a chance to reduce an incoming attack by 200 damage.", 0, 1);
        this.AddItemInfo("", 0, 1);
    }
}


ItemExamineUI.prototype.Appraisal_ShowActivationRequirements = function () {
    var iDifficulty = this.InqInt(0x6D);
    var activationReqsString = "";
    if (iDifficulty > 0) {
        activationReqsString = this.AppendHelper(activationReqsString, "Arcane Lore: " + iDifficulty);
    }

    var iRank = this.InqInt(0x6E);
    if (iRank > 0) {
        activationReqsString = this.AppendHelper(activationReqsString, "Allegiance Rank: " + iRank);
    }

    var heritage_req = this.InqInt(0xBC);
    if (heritage_req !== false) {
        activator_name = this.InqHeritageGroupDisplayName(heritage_req);
        activationReqsString = this.AppendHelper(activationReqsString, activator_name);
    }

    var iSkillLimit = this.InqInt(0x73);
    var attribute2nd = this.InqInt(0xB0);
    if (iSkillLimit > 0 && attribute2nd !== false) {
        var activator_name = this.InqSkillName(attribute2nd);
        if (activator_name !== false) {
            var numtxt = "{0}: {1}".format(activator_name, iSkillLimit);
            activationReqsString = this.AppendHelper(activationReqsString, numtxt);
        }
    }

    var iAttributeLimit = this.InqInt(0x102);
    attribute2nd = this.InqInt(0x101);
    if (iAttributeLimit > 0 && attribute2nd !== false) {
        var activator_name = this.InqAttributeName(attribute2nd);
        if (activator_name !== false) {
            var numtxt = "{0}: {1}".format(activator_name, iAttributeLimit);
            activationReqsString = this.AppendHelper(activationReqsString, numtxt);
        }
    }

    var iAttribute2ndLimit = this.InqInt(0x104);
    attribute2nd = this.InqInt(0x103);
    if (iAttribute2ndLimit > 0 && attribute2nd !== false) {
        var activator_name = this.InqAttribute2ndName(attribute2nd);
        if (activator_name !== false) {
            var numtxt = "{0}: {1}".format(activator_name, iAttribute2ndLimit);
            activationReqsString = this.AppendHelper(activationReqsString, numtxt);
        }
    }

    if (activationReqsString != "") {
        activationReqsString = "Activation requires " + activationReqsString;
        this.AddItemInfo(activationReqsString, 0, 1);
    }

    var has_allowed_activator = this.InqBool(0x5E);
    if (has_allowed_activator == 1) {
        var activator_name = "the original owner";
        var activator_string = this.InqString(0x19);
        activationReqsString = "This item can only be activated by ";
        if (activator_string !== false) {
            this.AddItemInfo(activationReqsString + activator_string + '.', 0, 1);
        } else {
            this.AddItemInfo(activationReqsString + activator_name + '.', 0, 1);
        }
    }
}

ItemExamineUI.prototype.Appraisal_ShowCasterData = function () {
    var rManaCModifier = this.InqFloat(0x90);
    if (rManaCModifier !== false) {
        rManaCModifier = rManaCModifier + 1.0;
        var szBuff = "Bonus to Mana Conversion: {0}.".format(this.ModifierToString(rManaCModifier));
        this.AddItemInfo(szBuff, 0, 0);
    }

    var eleDamageModPvM = this.InqFloat(0x98);
    if (eleDamageModPvM !== false) {
        var damageType = this.InqInt(0x2D);
        var tmp_txt = this.DamageTypeToString(damageType);
        var eleDamageModInfo = "Damage bonus for {0} spells:".format(tmp_txt);
        var eleDamageModPvP = this.GetElementalModPKModifier(eleDamageModPvM);
        var eleDamageModTextPvM = " vs. Monsters: {0}.".format(this.SmallModifierToString(eleDamageModPvM));
        this.AddItemInfo(eleDamageModInfo, 0, 0);
        this.AddItemInfo(eleDamageModTextPvM, 0, 1);

        var v7 = this.SmallModifierToString(eleDamageModPvP);
        this.AddItemInfo(" vs. Players: {0}.".format(v7), 0, 1);
    }
}

ItemExamineUI.prototype.Appraisal_ShowBoostValue = function () {
    //TODO - Check WeenieType != Healer (28)
    var iBoostAmount = this.InqInt(0x5A);
    var iAttribEffected = this.InqInt(0x59);
    var numtxt = "";
    if (iBoostAmount !== false && iAttribEffected !== false) {
        if (iAttribEffected == 2) {
            if (iBoostAmount < 0) {
                iBoostAmount = iBoostAmount * -1;
                numtxt = "Depletes " + iBoostAmount + " Health when used.";
            } else {
                numtxt = "Restores " + iBoostAmount + " Health when used.";
            }
        } else if (iAttribEffected == 4) {
            if (iBoostAmount < 0) {
                iBoostAmount = iBoostAmount * -1;
                numtxt = "Depletes " + iBoostAmount + " Stamina when used.";
            } else {
                numtxt = "Restores " + iBoostAmount + " Stamina when used.";
            }
        } else if (iAttribEffected == 6) {
            if (iBoostAmount < 0) {
                iBoostAmount = iBoostAmount * -1;
                numtxt = "Depletes " + iBoostAmount + " Mana when used.";
            } else {
                numtxt = "Restores " + iBoostAmount + " Mana when used.";
            }
        }
        if (numtxt != "")
            this.AddItemInfo(numtxt, 0, 0);
    }
}

ItemExamineUI.prototype.Appraisal_ShowHealKitValues = function () {
    //TODO - Check WeenieType == Healer (28)
    var weenieType = this.InqWeeniType();
    if (weenieType == 28) {
        var iBoostAmount = this.InqInt(0x5A);
        if (iBoostAmount !== false) {
            numtxt = "Bonus to Healing Skill: " + iBoostAmount;
            this.AddItemInfo(numtxt, 0, 0);
        }

        var rHealKitMod = this.InqFloat(0x64);
        if (rHealKitMod !== false) {
            var numtxt = "Restoration Bonus: {0}%".format(rHealKitMod * 100.0);
            this.AddItemInfo(numtxt, 0, 1);
        }
    }
}

ItemExamineUI.prototype.Appraisal_ShowCapacity = function () {
    var _itemsCapacity = this.InqInt(6);
    var _containersCapacity = this.InqInt(7);
    var numtxt = "";
    if ((_itemsCapacity === false || _itemsCapacity <= 0) && _containersCapacity > 0) {
        numtxt = "Can hold up to {0} containers.".format(_containersCapacity);
    } else if ((_containersCapacity === false || _containersCapacity <= 0) && _itemsCapacity !== false && _itemsCapacity > 0) {
        numtxt = "Can hold up to {0} items.".format(_itemsCapacity);
    } else if (_itemsCapacity !== false && _containersCapacity !== false && _itemsCapacity > 0 && _containersCapacity > 0) {
        numtxt = "Can hold up to {0} items and {0} containers.".format(_itemsCapacity, _containersCapacity);
    }
    if (numtxt != "")
        this.AddItemInfo(numtxt, 0, 0);

    var iMaxPages = this.InqInt(0xAF);
    var iNumPages = this.InqInt(0xAE);
    if (iMaxPages !== false && iNumPages !== false) {
        this.AddItemInfo("{0} of {1} pages full.".format(iNumPages, iMaxPages), 0, 0);;
    }
}

ItemExamineUI.prototype.Appraisal_ShowLockAppraiseInfo = function () {
    var locked = this.InqBool(3);
    if (locked === 1) {
        this.AddItemInfo("Locked", 0, 0);
        var iStat = this.InqInt(0x26); // RESIST_LOCKPICK_INT 
        //var lsp = this.InqInt(0xAD); // APPRAISAL_LOCKPICK_SUCCESS_PERCENT_INT 
        var lsp = 1; // This is the success chance to pick calculated by the server. Let's just call it 50%.
        if (iStat !== false) {
            if (lsp !== false) {
                this.AddItemInfo("The lock looks %s to pick (Resistance {0}).".format(iStat), 0, 0); // We'll just leave the %s.
            } else {
                this.AddItemInfo("You can't tell how hard the lock is to pick.", 0, 0);
            }
        }
    } else if (locked === 0) {
        this.AddItemInfo("Unlocked", 0, 0);
    } else {
        var iStat = this.InqInt(0x26);
        if (iStat !== false) {
            if (iStat > 0) {
                this.AddItemInfo("Bonus to Lockpick Skill: +" + iStat, 0, 0);
            } else if (iStat != 0) {
                this.AddItemInfo("Bonus to Lockpick Skill: " + iStat, 0, 0);
            }
        }
    }
}

// ItemExamineUI::Appraisal_ShowManaStoneInfo(v3, v9);
ItemExamineUI.prototype.Appraisal_ShowManaStoneInfo = function () {
    var spells = this.InqSpells();

    if (spells.length == 0 || spells == false) {
        var iCurMana = this.InqInt(0x6B); // ITEM_CUR_MANA_INT
        if (iCurMana !== false)
            this.AddItemInfo("Stored Mana: " + iCurMana, 0, 1);

        var rEfficiency = this.InqFloat(0x57);
        if (rEfficiency !== false)
            this.AddItemInfo("Efficiency: {0}%".format(parseInt(rEfficiency * 100.0)), 0, 1);

        var rChanceOfDestruction = this.InqFloat(0x89);
        if (rChanceOfDestruction !== false)
            this.AddItemInfo("Chance of Destruction: {0}%".format(parseInt(rChanceOfDestruction * 100.0)), 0, 1);
    }
}

// ItemExamineUI::Appraisal_ShowRemainingUses(v3, v9);
ItemExamineUI.prototype.Appraisal_ShowRemainingUses = function () {
    var iKeys = this.InqInt(0xC1);
    if (iKeys !== false) {
        if (iKeys == 1) {
            var numtxt = "Contains 1 key.";
        } else {
            var numtxt = "Contains {0} keys.".format(iKeys);
        }
        this.AddItemInfo(numtxt, 0, 1);
    }

    var unlimited_use = this.InqBool(0x3F);
    var iUses = this.InqInt(0x5C);
    if (unlimited_use == 1)
        this.AddItemInfo("Number of uses remaining:  Unlimited", 0, 1);
    else if (iUses !== false)
        this.AddItemInfo("Number of uses remaining: " + iUses, 0, 1);

    // TODO - Check if bitfield = 0x30000 (Healer | Lockpick)??
}

// ItemExamineUI::Appraisal_ShowCraftsman(v3, v9);
ItemExamineUI.prototype.Appraisal_ShowCraftsman = function () {
    // var has_allowed_wielder_iid = this.InqBool(85);
    // var has_allowed_activator_iid = this.InqBool(94);
    // TODO The above has some additional checks for stuff

    var strCraftsman = this.InqString(0x19);
    if (strCraftsman !== false)
        this.AddItemInfo("Created by {0}.".format(strCraftsman), 0, 1);
}

ItemExamineUI.prototype.Appraisal_ShowRareInfo = function () {
    var rareUsesTimer = this.InqBool(0x6C);
    if (rareUsesTimer == 1)
        this.AddItemInfo("This rare item has a timer restriction of 3 minutes. You will not be able to use another rare item with a timer within 3 minutes of using this one.", 0, 0);

    var iRareID = this.InqInt(0x11);
    if (iRareID !== false)
        this.AddItemInfo("Rare #" + iRareID, 0, 0);
}

ItemExamineUI.prototype.Appraisal_ShowMagicInfo = function () {
    var strSpellDescriptions = "Spell Descriptions:";
    var strEnchantments = "Enchantments:\n";

    var spells = this.InqSpells();
    if (spells.length > 0) {
        var fAddedSpell = true;
        // TODO - GET SPELL DESCRIPTIONS!
        //var spells = this.GetSpellNamesAndDescriptions();

        if (spells !== false) {
            iSpellcraft = this.InqInt(0x6A);
            if (iSpellcraft !== false)
                this.AddItemInfo("Spellcraft: {0}.".format(iSpellcraft), 0, 1);

            var iCurMana = this.InqInt(0x6B);
            var iManaCost = this.InqInt(0x6C);
            if (iCurMana !== false && iManaCost !== false)
                this.AddItemInfo("Mana: {0} / {1}.".format(iCurMana, iManaCost), 0, 1);

            var rManaRate = this.InqFloat(5); //MANA_RATE_FLOAT 
            var iManaCost = this.InqInt(0x75); //ITEM_MANA_COST_INT
            if (rManaRate !== false) {
                var v30 = 1.0 / rManaRate;
                if (1.0 / rManaRate < 0.0) v30 = v30 * -1;
                this.AddItemInfo("Mana Cost: 1 point per {0} seconds.".format(parseInt(v30 + 0.5)), 0, 1);
            } else if (iManaCost !== false) {
                if (iManaCost <= 0)
                    var numtxt = "Mana Cost: {0}.".format(iManaCost);
                else
                    var numtxt = "Mana Cost: {0}.\n(Can be reduced by the Mana Conversion skill)".format(iManaCost);
                this.AddItemInfo(numtxt, 0, 0);
            }
			/*
			foreach($spells as $s)
				$strSpellDescriptions.="\n~ ".$s;

			this.AddItemInfo($strSpellDescriptions, 0, 0);
			*/
        }
    }
}

ItemExamineUI.prototype.Appraisal_ShowDescription = function () {
    var lifespan = this.InqInt(0x10B);
    var creationTime = this.InqInt(0x62);
    var remaining_lifespan = this.InqInt(0x10C);
    if (lifespan !== false && creationTime !== false && remaining_lifespan !== false) {
        var strAppend = "";
        if (remaining_lifespan > 0) {
            strAppend = "This item expires in ";
            var remaining_lifespan_str = this.DeltaTimeToString(remaining_lifespan);
            strAppend += remaining_lifespan_str;
        } else {
            strAppend = "This item is in the act of disintegrating.";
        }

        this.AddItemInfo(strAppend, 0, 1);
    }

    var strAppend = "";
    var strDesc = this.InqString(0x10);
    if (strDesc !== false) {
        var gearPlatingName = this.InqString(0x34);
        if (gearPlatingName !== false)
            strAppend += gearPlatingName + "\n"; // Technically, this is not a +=, but that seems like a client bug

        var iDecoration = this.InqInt(0xAC);
        if (iDecoration !== false) {
            var iWorkmanship = this.InqInt(0x69);
            var iGemCount = this.InqInt(177);
            if ((iDecoration & 1) > 0 && $iWorkmanship !== false) {
                var strMaterial = this.InqWorkmanshipAdjective(iWorkmanship, iGemCount);
                strAppend += strMaterial + ' ';
            }
        }
        var iMaterial = this.InqInt(0x83);
        if (iMaterial !== false) {
            strMaterial = this.InqMaterialName(iMaterial);
            strAppend += strMaterial + ' ';
        }

        strAppend += strDesc + ' ';

        var iGemCount = this.InqInt(0xB1);
        var iGemType = this.InqInt(0xB2);
        if ((iDecoration & 4) > 0 && iGemCount !== false && iGemType !== false) {
            if (iGemCount == 1)
                var strMaterial = this.InqMaterialName(iGemType);
            else
                var strMaterial = this.InqPluralizedGemName(iGemType);

            var strAppend = strAppend.trim() + ', set with ' + iGemCount + ' ' + strMaterial;
        }

        this.AddItemInfo("", 0, 1);
        this.AddItemInfo(strAppend, 0, 1);
    } else {
        var strDesc = this.InqString(0xF);
        if (strDesc !== false) {
            this.AddItemInfo("", 0, 1);
            this.AddItemInfo(strDesc, 0, 1);
        }
    }

    // Portal Stuff
    var bitfield = this.InqInt(0x6F);
    if (bitfield !== false) {
        var portalDesc = "";
        if ((bitfield & 2) > 0)
            portalDesc += "Player Killers may not use this portal.\n";
        if ((bitfield & 4) > 0)
            portalDesc += "Lite Player Killers may not use this portal.\n";
        if ((bitfield & 8) > 0)
            portalDesc += "Non-Player Killers may not use this portal.\n";
        if ((bitfield & 0x20) > 0)
            portalDesc += "This portal cannot be recalled nor linked to.\n";
        if ((bitfield & 0x10) > 0)
            portalDesc += "This portal cannot be summoned.\n";

        this.AddItemInfo("", 0, 1);
        this.AddItemInfo(portalDesc, 0, 1);
    }

    var cost = this.InqInt64(3);
    if (cost !== false)
        this.AddItemInfo("Using this gem will drain " + cost.toLocaleString() + " of your available experience.", 0, 1);
}
