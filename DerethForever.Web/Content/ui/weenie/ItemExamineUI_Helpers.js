/*
// This is just to make my life easier.
*/
String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+i+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

ItemExamineUI.prototype.InqWeeniType = function(key){
	if(!weenieStore.weenie.WeenieTypeId) return false;
	return weenieStore.weenie.WeenieTypeId;
}

ItemExamineUI.prototype.InqBool = function(key){
	if(!weenieStore.weenie.boolStats) return false;
	for(var i = 0;i<weenieStore.weenie.boolStats.length;i++){
		if(weenieStore.weenie.boolStats[i].key == key){
			return weenieStore.weenie.boolStats[i].value;
		}
	}
	return false;
}

ItemExamineUI.prototype.InqDataId = function(key){
	if(!weenieStore.weenie.didStats) return false;
	for(var i = 0;i<weenieStore.weenie.didStats.length;i++){
		if(weenieStore.weenie.didStats[i].key == key){
			return weenieStore.weenie.didStats[i].value;
		}
	}
	return false;
}

ItemExamineUI.prototype.InqFloat = function(key){
	if(!weenieStore.weenie.floatStats) return false;
	for(var i = 0;i<weenieStore.weenie.floatStats.length;i++){
		if(weenieStore.weenie.floatStats[i].key == key){
			return weenieStore.weenie.floatStats[i].value;
		}
	}
	return false;
}

ItemExamineUI.prototype.InqInt = function(key){
	if(!weenieStore.weenie.intStats) return false;
	for(var i = 0;i<weenieStore.weenie.intStats.length;i++){
		if(weenieStore.weenie.intStats[i].key == key){
			return weenieStore.weenie.intStats[i].value;
		}
	}
	return false;
}

ItemExamineUI.prototype.InqInt64 = function(key){
	if(!weenieStore.weenie.int64Stats) return false;
	for(var i = 0;i<weenieStore.weenie.int64Stats.length;i++){
		if(weenieStore.weenie.int64Stats[i].key == key){
			return weenieStore.weenie.int64Stats[i].value;
		}
	}
	return false;
}

ItemExamineUI.prototype.InqString = function(key){
	if(!weenieStore.weenie.stringStats) return false;
	for(var i = 0;i<weenieStore.weenie.stringStats.length;i++){
		if(weenieStore.weenie.stringStats[i].key == key){
			return weenieStore.weenie.stringStats[i].value;
		}
	}
	return false;
}

ItemExamineUI.prototype.InqSpells = function(key){
	if(!weenieStore.weenie.spellbook) return false;
    var spells = [];
	for(var i = 0;i<weenieStore.weenie.spellbook.length;i++){
		//if(weenieStore.weenie.spellbook[i].Deleted == false){
            spells.push(weenieStore.weenie.spellbook[i].key);
		//}
	}
    if(spells.length > 0)
        return spells;
	return false;
}

ItemExamineUI.prototype.InqEnumValue = function (enums, key) {
    if (enums == null) return "Unknown (" + key + ")";
    for (var i = 0; i < enums.length; i++) {
        if (enums[i].Key == key) {
            return enums[i].Value;
        }
    }
    return false;
}
ItemExamineUI.prototype.InqSpellEnum = function (enums, key) {
    if (enums == null) return "Unknown Spell (" + key + ")";
    for (var i = 0; i < enums.length; i++) {
        if (enums[i].id == key) {
            return enums[i].name;
        }
    }
    return false;
}

//ItemExamineUI.prototype.InqString = function(key){
//	if(!weenieStore.weenie.stringStats) return false;
//	for(var i = 0;i<weenieStore.weenie.stringStats.length;i++){
//		if(weenieStore.weenie.stringStats[i].key == key){
//			return weenieStore.weenie.stringStats[i].value;
//		}
//	}
//	return false;
//}

ItemExamineUI.prototype.InqAttribute = function(key){
	if(!weenieStore.weenie.attributes) return false;
	if(typeof weenieStore.weenie.attributes[key] != "undefined"){
		return weenieStore.weenie.attributes[key];
	}
	return false;
}

ItemExamineUI.prototype.InqMaterialName = function (key) {
    var materialEnum = JSON.parse(localStorage.getItem("Material"));
    return this.InqEnumValue(materialEnum, key);
}

ItemExamineUI.prototype.GetArmorSet = function(setID){
	var SETS = [];
	SETS[1] = "SpellSetID_EnchantmentVersion";
	SETS[2] = "Test_EquipmentSet";
	SETS[4] = "Carraida's Benediction";
	SETS[5] = "Noble Relic";
	SETS[6] = "Ancient Relic";
	SETS[7] = "Relic Alduressa";
	SETS[8] = "Shou-jen";
	SETS[9] = "Empyrean Rings";
	SETS[10] = "Arm, Mind, Heart";
	SETS[11] = "Coat of the Perfect Light";
	SETS[12] = "Leggings of Perfect Light";
	SETS[13] = "Soldier's";
	SETS[14] = "Adept's";
	SETS[15] = "Archer's";
	SETS[16] = "Defender's";
	SETS[17] = "Tinker's";
	SETS[18] = "Crafter's";
	SETS[19] = "Hearty";
	SETS[20] = "Dexterous";
	SETS[21] = "Wise";
	SETS[22] = "Swift";
	SETS[23] = "Hardenend";
	SETS[24] = "Reinforced";
	SETS[25] = "Interlocking";
	SETS[26] = "Flame Proof";
	SETS[27] = "Acid Proof";
	SETS[28] = "Cold Proof";
	SETS[29] = "Lightning Proof";
	SETS[30] = "Dedication";
	SETS[31] = "Gladiatorial Clothing";
	SETS[32] = "Ceremonial Clothing";
	SETS[33] = "Protective Clothing";
	SETS[34] = "NoobieArmor_EquipmentSet";
	SETS[35] = "Sigil of Defense";
	SETS[36] = "Sigil of Destruction";
	SETS[37] = "Sigil of Fury";
	SETS[38] = "Sigil of Growth";
	SETS[39] = "Sigil of Vigor";
	SETS[40] = "Heroic Protector";
	SETS[41] = "Heroic Destroyer";
	SETS[42] = "OlthoiArmorDRed_Set";
	SETS[43] = "OlthoiArmorCRat_Set";
	SETS[44] = "OlthoiArmorCRed_Set";
	SETS[45] = "OlthoiArmorDRat_Set";
	SETS[46] = "AlduressaRelicUpgrade_EquipmentSet";
	SETS[47] = "Upgraded Ancient Relic";
	SETS[48] = "NobleRelicUpgrade_EquipmentSet";
	SETS[49] = "Weave of Alchemy";
	SETS[50] = "Weave of Arcane Lore";
	SETS[51] = "Weave of Armor Tinkering";
	SETS[52] = "Weave of Assess Person";
	SETS[53] = "Weave of Light Weapons";
	SETS[54] = "Weave of Missile Weapons";
	SETS[55] = "Weave of Cooking";
	SETS[56] = "Weave of Creature Enchantment";
	SETS[57] = "Weave of Missile Weapons";
	SETS[58] = "Weave of Finesse";
	SETS[59] = "Weave of Deception";
	SETS[60] = "Weave of Fletching";
	SETS[61] = "Weave of Healing";
	SETS[62] = "Weave of Item Enchantment";
	SETS[63] = "Weave of Item Tinkering";
	SETS[64] = "Weave of Leadership";
	SETS[65] = "Weave of Life Magic";
	SETS[66] = "Weave of Loyalty";
	SETS[67] = "Weave of Light Weapons";
	SETS[68] = "Weave of Magic Defense";
	SETS[69] = "Weave of Magic Item Tinkering";
	SETS[70] = "Weave of Mana Conversion";
	SETS[71] = "Weave of Melee Defense";
	SETS[72] = "Weave of Missile Defense";
	SETS[73] = "Weave of Salvaging";
	SETS[74] = "Weave of Light Weapons";
	SETS[75] = "Weave of Light Weapons";
	SETS[76] = "Weave of Heavy Weapons";
	SETS[77] = "Weave of Missile Weapons";
	SETS[78] = "Weave of Two Handed Combat";
	SETS[79] = "Weave of Light Weapons";
	SETS[80] = "Weave of Void Magic";
	SETS[81] = "Weave of War Magic";
	SETS[82] = "Weave of Weapon Tinkering";
	SETS[83] = "Weave of Assess Creature ";
	SETS[84] = "Weave of Dirty Fighting";
	SETS[85] = "Weave of Dual Wield";
	SETS[86] = "Weave of Recklessness";
	SETS[87] = "Weave of Shield";
	SETS[88] = "Weave of Sneak Attack";
	SETS[89] = "Shou-Jen Shozoku";
	SETS[90] = "Weave of Summoning";
	SETS[91] = "Shrouded Soul";
	SETS[92] = "Darkened Mind";
	SETS[93] = "Clouded Spirit";
	SETS[94] = "Minor Stinging Shrouded Soul";
	SETS[95] = "Minor Sparking Shrouded Soul";
	SETS[96] = "Minor Smoldering Shrouded Soul";
	SETS[97] = "Minor Shivering Shrouded Soul";
	SETS[98] = "Minor Stinging Darkened Mind";
	SETS[99] = "Minor Sparking Darkened Mind";
	SETS[100] = "Minor Smoldering Darkened Mind";
	SETS[101] = "Minor Shivering Darkened Mind";
	SETS[102] = "Minor Stinging Clouded Spirit";
	SETS[103] = "Minor Sparking Clouded Spirit";
	SETS[104] = "Minor Smoldering Clouded Spirit";
	SETS[105] = "Minor Shivering Clouded Spirit";
	SETS[106] = "Major Stinging Shrouded Soul";
	SETS[107] = "Major Sparking Shrouded Soul";
	SETS[108] = "Major Smoldering Shrouded Soul";
	SETS[109] = "Major Shivering Shrouded Soul";
	SETS[110] = "Major Stinging Darkened Mind";
	SETS[111] = "Major Sparking Darkened Mind";
	SETS[112] = "Major Smoldering Darkened Mind";
	SETS[113] = "Major Shivering Darkened Mind";
	SETS[114] = "Major Stinging Clouded Spirit";
	SETS[115] = "Major Sparking Clouded Spirit";
	SETS[116] = "Major Smoldering Clouded Spirit";
	SETS[117] = "Major Shivering Clouded Spirit";
	SETS[118] = "Blackfire Stinging Shrouded Soul";
	SETS[119] = "Blackfire Sparking Shrouded Soul";
	SETS[120] = "Blackfire Smoldering Shrouded Soul";
	SETS[121] = "Blackfire Shivering Shrouded Soul";
	SETS[122] = "Blackfire Stinging Darkened Mind";
	SETS[123] = "Blackfire Sparking Darkened Mind";
	SETS[124] = "Blackfire Smoldering Darkened Mind";
	SETS[125] = "Blackfire Shivering Darkened Mind";
	SETS[126] = "Blackfire Stinging Clouded Spirit";
	SETS[127] = "Blackfire Sparking Clouded Spirit";
	SETS[128] = "Blackfire Smoldering Clouded Spirit";
	SETS[129] = "Blackfire Shivering Clouded Spirit";
	SETS[130] = "Shimmering Shadows";
	if(SETS.hasOwnProperty(setID)){
		return SETS[setID];
	}
	
	return false;
}
ItemExamineUI.prototype.InqSkillName = function(skillID){
	return this.getSkill(skillID);
}
ItemExamineUI.prototype.getSkill = function(skillID){
	var SKILLS = [];
	SKILLS[0] = "None";
	SKILLS[1] = "Axe";
	SKILLS[2] = "Bow";
	SKILLS[3] = "Crossbow";
	SKILLS[4] = "Dagger";
	SKILLS[5] = "Mace";
	SKILLS[6] = "Melee Defense";
	SKILLS[7] = "Missile Defense";
	SKILLS[9] = "Spear";
	SKILLS[10] = "Staff";
	SKILLS[11] = "Sword";
	SKILLS[12] = "Thrown Weapons";
	SKILLS[13] = "Unarmed Combat";
	SKILLS[14] = "Arcane Lore";
	SKILLS[15] = "Magic Defense";
	SKILLS[16] = "Mana Conversion";
	SKILLS[18] = "Item Tinkering";
	SKILLS[19] = "Assess Person";
	SKILLS[20] = "Deception";
	SKILLS[21] = "Healing";
	SKILLS[22] = "Jump";
	SKILLS[23] = "Lockpick";
	SKILLS[24] = "Run";
	SKILLS[27] = "Assess Creature";
	SKILLS[28] = "Weapon Tinkering";
	SKILLS[29] = "Armor Tinkering";
	SKILLS[30] = "Magic Item Tinkering";
	SKILLS[31] = "Creature Enchantment";
	SKILLS[32] = "Item Enchantment";
	SKILLS[33] = "Life Magic";
	SKILLS[34] = "War Magic";
	SKILLS[35] = "Leadership";
	SKILLS[36] = "Loyalty";
	SKILLS[37] = "Fletching";
	SKILLS[38] = "Alchemy";
	SKILLS[39] = "Cooking";
	SKILLS[40] = "Salvaging";
	SKILLS[41] = "Two Handed Combat";
	SKILLS[43] = "Void";
	SKILLS[44] = "Heavy Weapons";
	SKILLS[45] = "Light Weapons";
	SKILLS[46] = "Finesse Weapons";
	SKILLS[47] = "Missile Weapons";
	SKILLS[48] = "Shield";
	SKILLS[49] = "Dual Wield";
	SKILLS[50] = "Recklessness";
	SKILLS[51] = "Sneak Attack";
	SKILLS[52] = "Dirty Fighting";
	SKILLS[54] = "Summoning";
	if(SKILLS.hasOwnProperty(skillID)){
		return SKILLS[skillID];
	}
	
	return false;

}
ItemExamineUI.prototype.DamageTypeToString = function(dtype){
	var DAMAGE_TYPE = [];
	DAMAGE_TYPE[0x1] = "Slashing";
	DAMAGE_TYPE[0x2] = "Piercing";
	DAMAGE_TYPE[0x4] = "Bludgeoning";
	DAMAGE_TYPE[0x8] = "Cold";
	DAMAGE_TYPE[0x10] = "Fire";
	DAMAGE_TYPE[0x20] = "Acid";
	DAMAGE_TYPE[0x40] = "Electrical";
	DAMAGE_TYPE[0x80] = "Health";
	DAMAGE_TYPE[0x100] = "Stamina";
	DAMAGE_TYPE[0x200] = "Mana";
	DAMAGE_TYPE[0x400] = "Nether";
	DAMAGE_TYPE[0x10000000] = "Prismatic";
	
	var DamageString = "";
	for (var key in DAMAGE_TYPE) {
		if((key & dtype) > 0){
			if(DamageString != ""){
				DamageString+= '/'+DAMAGE_TYPE[key];
				return DamageString;
			}else{
				DamageString = DAMAGE_TYPE[key];
			}
		}
	}
	
	return DamageString;
}
ItemExamineUI.prototype.WeaponTimeToString = function(wtime){
	if(wtime < 11){
		return "Very Fast";
	}else if(wtime < 31){
		return "Fast";
	}else if(wtime < 50){
		return "Average";
	}else if(wtime < 80){
		return "Slow";
	}else{
		return "Very Slow";
	}
}
ItemExamineUI.prototype.ClothingPriorityToString = function(priority){
	var byte1 = (priority >> 8 & 0xFF);

	var cover = "";		
	if((priority & 0x4000) !=0){
		cover += "Head, ";	
	}
	if((priority & 8) != 0 || (priority & 0x400) !=0){
		cover += "Chest, ";	
	}
	if((priority & 0x10) != 0 || (byte1 & 8) !=0){
		cover += "Abdomen, ";	
	}
	if((priority & 0x20) != 0 || (byte1 & 0x10) !=0){
		cover += "Upper Arms, ";	
	}
	if((priority & 0x40) != 0 || (byte1 & 0x20) !=0){
		cover += "Lower Arms, ";	
	}
	if((priority & 0x2) != 0 || (byte1 & 0x1) !=0){
		cover += "Upper Legs, ";	
	}
	if((priority & 0x4) != 0 || (byte1 & 0x2) !=0){
		cover += "Lower Legs, ";	
	}
	if((priority & 0x10000) != 0){
		cover += "Feet, ";	
	}

	cover = cover.trim();
	if(cover.charAt(cover.length-1)==",") cover = cover.slice(0, -1); //remove any trailing commas
	if(cover != ""){
		return "Covers "+cover;
	}else{
		return false;
	}
}
ItemExamineUI.prototype.ModifierToString = function(rMod){
	var v1 = 1 - rMod;
	if(v1 < 0){
		v1 = Math.abs(v1);
	}
	var check = 2 * (rMod < 1.0) + 43;
	if( check < 0){
		mod_sign = "-";
	}else{
		mod_sign = "+";
	}

	var g_szPecentText = "{0}{1}%".format(mod_sign, parseInt(v1 * 100));
	return g_szPecentText;
}
ItemExamineUI.prototype.SmallModifierToString = function(val){
	var bonus = val - 1;
	var mod_sign;
	if( bonus < 0){
		mod_sign = "-";
		bonus = bonus * -1;
	}else{
		mod_sign = "+";
	}
	return (bonus * 100.0).toFixed(1);
}
ItemExamineUI.prototype.DamageResistanceToString  = function(dtype, al, modifier){
	var dTxt;
	switch(dtype){
		case 1: dTxt = "Slashing: "; break;       
		case 2: dTxt = "Piercing: "; break;       
		case 4: dTxt = "Bludgeoning: "; break;       
		case 8: dTxt = "Cold: "; break;       
		case 16: dTxt = "Fire: "; break;       
		case 32: dTxt = "Acid: "; break;       
		case 64: dTxt = "Electric: "; break;       
		case 1024: dTxt = "Nether: "; break;     
		default: return false;                
	}

	var modTxt;
	if(modifier >= 0){
		if(modifier >= 2){
			modTxt = "Unparalleled ";
			modifier = 2;
		}else if(modifier >= 1.6){
			modTxt = "Excellent";
		}else if(modifier >= 1.2){
			modTxt = "Above Average";
		}else if(modifier > 0.8){
			modTxt = "Average";
		}else if(modifier > 0.4){
			modTxt = "Below Average";
		}else if(modifier >= 0){
			modTxt = "Poor";
		}
	}

	return dTxt+modTxt+"  ({0})".format(parseInt(al*modifier));
}

ItemExamineUI.prototype.GetAppraisalStringFromRequirements = function(iReq, iSkill, iDiff){
	var strSkill = "";
	if ( iReq == 2 || iReq == 4 || iReq == 6 ) strSkill = "base ";
	switch ( iReq ){
		case 1:
		case 2:
		case 8: strSkill+= this.InqSkillName(iSkill); break;
		case 3:
		case 4: strSkill+= this.InqAttributeName(iSkill); break;
		case 5:
		case 6: strSkill+= this.InqAttribute2ndName(iSkill); break;
		case 9:
		case 10: 
			switch ( iSkill ){
				case 287: strSkill = "Standing with the Celestial Hand"; break;
				case 288: strSkill = "Standing with the Eldrytch Web"; break;
				case 289: strSkill = "Standing with the Radiant Blood"; break;
				default: strSkill = "unknown quality";
			}
			break;
		case 7: strSkill = "level"; break;
		case 11: strSkill = this.InqCreatureDisplayName(iDiff); break;
		case 12: strSkill = this.InqHeritageGroupDisplayName(iDiff); break;
		default: return false;
	}
	return strSkill;
}

ItemExamineUI.prototype.DeltaTimeToString = function(time){
	var v2 = time / 0x278D00; // months
	var v3 = time % 0x278D00 / 0x15180; // days
	var v4 = time % 0x278D00 % 0x15180;
	var v5 = v4 / 0xE10; // hours
	v4 %= 0xE10; 
	var v12 = v4 % 0x3C; // seconds
	var v13 = v4 / 0x3C; // minutes
	var buf = "";
	if(parseInt(v2) > 0)
		buf+= parseInt(v2) + "mo ";
	if(parseInt(v3) > 0)
		buf+= parseInt(v3) + "d ";
	if(parseInt(v5) > 0)
		buf+= parseInt(v5) + "h ";
	if(parseInt(v13) > 0)
		buf+= parseInt(v13) + "m ";
	buf+= parseInt(v12) + "s ";
	
	return buf;     
}

ItemExamineUI.prototype.InqWorkmanshipAdjective = function(wlevel, isGem){
	if(wlevel > 10){
		wlevel = 10;
	}
	var workmanship = this.__getWorkmanship(wlevel);
	if(wlevel <= 4){
		if(isGem==true){
			workmanship += "cut";
		}else{
			workmanship += "crafted";
		}
	}
	return workmanship;
}
	
ItemExamineUI.prototype.__getWorkmanship = function(workmanship){
	switch(workmanship){
		case 1: return('Poorly ');
		case 2: return('Well-');
		case 3: return('Finely ');
		case 4: return('Exquisitely ');
		case 5: return('Magnificent');
		case 6: return('Nearly flawless');
		case 7: return('Flawless');
		case 8: return('Utterly flawless');
		case 9: return('Incomparable');
		case 10: return('Priceless');
	}
}

ItemExamineUI.prototype.ItemTotalXPToLevel = function(_gained_xp, _base_xp, _max_level, _xp_scheme){
	var level = 0;
	switch (_xp_scheme){
		case 1: //ItemXpStyle.Fixed:
			level = Math.floor(_gained_xp / _base_xp);
			break;
		case 2: //ItemXpStyle.ScalesWithLevel:
			var levelXP = _base_xp;
			var remainXP = _gained_xp;

			while (remainXP >= levelXP){
				level++;
				remainXP -= levelXP;
				levelXP *= 2;
			}
			break;
		case 3: //ItemXpStyle.FixedPlusBase:
			if (_gained_xp >= _base_xp && _gained_xp < _base_xp * 3)
				level = 1;
			else
				level = Math.floor((_gained_xp - _base_xp) / _base_xp);
			break;
	}

	if (level > _max_level)
		level = _max_level;

	return level;
}

ItemExamineUI.prototype.ItemLevelToTotalXP = function(itemLevel, baseXP, maxLevel, xpScheme){
	if (itemLevel < 1)
		return 0;

	if (itemLevel > maxLevel)
		itemLevel = maxLevel;

	if (itemLevel == 1)
		return baseXP;

	switch (xpScheme){
		case 1: //ItemXpStyle.Fixed:
			return itemLevel * baseXP;

		case 2:// ItemXpStyle.ScalesWithLevel:
		default:
			var levelXP = baseXP;
			var totalXP = baseXP;

			for (var i = itemLevel - 1; i > 0; i--){
				levelXP *= 2;
				totalXP += levelXP;
			}

			return totalXP;
		case 3://ItemXpStyle.FixedPlusBase:
			return itemLevel * baseXP + baseXP;
	}
}

ItemExamineUI.prototype.InqCreatureDisplayName = function(id){
	switch(id){
		case 1: return "Olthoi"; break;
		case 2: return "Banderling"; break;
		case 3: return "Drudge"; break;
		case 4: return "Mosswart"; break;
		case 5: return "Lugian"; break;
		case 6: return "Tumerok"; break;
		case 7: return "Mite"; break;
		case 8: return "Tusker"; break;
		case 9: return "Phyntos_Wasp"; break;
		case 10: return "Rat"; break;
		case 11: return "Auroch"; break;
		case 12: return "Cow"; break;
		case 13: return "Golem"; break;
		case 14: return "Undead"; break;
		case 15: return "Gromnie"; break;
		case 16: return "Reedshark"; break;
		case 17: return "Armoredillo"; break;
		case 18: return "Fae"; break;
		case 19: return "Virindi"; break;
		case 20: return "Wisp"; break;
		case 21: return "Knathtead"; break;
		case 22: return "Shadow"; break;
		case 23: return "Mattekar"; break;
		case 24: return "Mumiyah"; break;
		case 25: return "Rabbit"; break;
		case 26: return "Sclavus"; break;
		case 27: return "Shallows_Shark"; break;
		case 28: return "Monouga"; break;
		case 29: return "Zefir"; break;
		case 30: return "Skeleton"; break;
		case 31: return "Human"; break;
		case 32: return "Shreth"; break;
		case 33: return "Chittick"; break;
		case 34: return "Moarsman"; break;
		case 35: return "Olthoi_Larvae"; break;
		case 36: return "Slithis"; break;
		case 37: return "Deru"; break;
		case 38: return "Fire_Elemental"; break;
		case 39: return "Snowman"; break;
		case 40: return "Unknown"; break;
		case 41: return "Bunny"; break;
		case 42: return "Lightning_Elemental"; break;
		case 43: return "Rockslide"; break;
		case 44: return "Grievver"; break;
		case 45: return "Niffis"; break;
		case 46: return "Ursuin"; break;
		case 47: return "Crystal"; break;
		case 48: return "Hollow_Minion"; break;
		case 49: return "Scarecrow"; break;
		case 50: return "Idol"; break;
		case 51: return "Empyrean"; break;
		case 52: return "Hopeslayer"; break;
		case 53: return "Doll"; break;
		case 54: return "Marionette"; break;
		case 55: return "Carenzi"; break;
		case 56: return "Siraluun"; break;
		case 57: return "Aun_Tumerok"; break;
		case 58: return "Hea_Tumerok"; break;
		case 59: return "Simulacrum"; break;
		case 60: return "Acid_Elemental"; break;
		case 61: return "Frost_Elemental"; break;
		case 62: return "Elemental"; break;
		case 63: return "Statue"; break;
		case 64: return "Wall"; break;
		case 65: return "Altered_Human"; break;
		case 66: return "Device"; break;
		case 67: return "Harbinger"; break;
		case 68: return "Dark_Sarcophagus"; break;
		case 69: return "Chicken"; break;
		case 70: return "Gotrok_Lugian"; break;
		case 71: return "Margul"; break;
		case 72: return "Bleached_Rabbit"; break;
		case 73: return "Nasty_Rabbit"; break;
		case 74: return "Grimacing_Rabbit"; break;
		case 75: return "Burun"; break;
		case 76: return "Target"; break;
		case 77: return "Ghost"; break;
		case 78: return "Fiun"; break;
		case 79: return "Eater"; break;
		case 80: return "Penguin"; break;
		case 81: return "Ruschk"; break;
		case 82: return "Thrungus"; break;
		case 83: return "Viamontian_Knight"; break;
		case 84: return "Remoran"; break;
		case 85: return "Swarm"; break;
		case 86: return "Moar"; break;
		case 87: return "Enchanted_Arms"; break;
		case 88: return "Sleech"; break;
		case 89: return "Mukkir"; break;
		case 90: return "Merwart"; break;
		case 91: return "Food"; break;
		case 92: return "Paradox_Olthoi"; break;
		case 93: return "Harvest"; break;
		case 94: return "Energy"; break;
		case 95: return "Apparition"; break;
		case 96: return "Aerbax"; break;
		case 97: return "Touched"; break;
		case 98: return "Blighted_Moarsman"; break;
		case 99: return "Gear_Knight"; break;
		case 100: return "Gurog"; break;
		case 101: return "A'nekshay"; break;	
		default: return false;
	}
}

ItemExamineUI.prototype.InqHeritageGroupDisplayName = function(id){
	switch(id){
		case 1: return "Aluvian"; break;       
		case 2: return "Gharu'ndim"; break;       
		case 3: return "Sho"; break;       
		case 4: return "Viamontian"; break;       
		case 5: return "Umbraen"; break;       
		case 6: return "Gearknight"; break;       
		case 7: return "Tumerok"; break;       
		case 8: return "Lugian"; break;       
		case 9: return "Empyrean"; break;       
		case 10: return "Penumbraen"; break;       
		case 11: return "Undead"; break;       
		case 12:
		case 13: return "Olthoi"; break;       
	}
	return false;
}

ItemExamineUI.prototype.InqAttributeName = function(stype){
	switch(stype){
		case 1: return "Strength";
		case 2: return "Endurance";
		case 3: return "Quickness";
		case 4: return "Coordination";
		case 5: return "Focus";
		case 6: return "Self";
		default: return false;
	}
}

ItemExamineUI.prototype.InqAttribute2ndName = function(stype){
	switch(stype){
		case 1: return "Maximum Health";
		case 2: return "Health";
		case 3: return "Maximum Stamina";
		case 4: return "Stamina";
		case 5: return "Maximum Mana";
		case 6: return "Mana";
		default: return false;
	}
}
ItemExamineUI.prototype.GetElementalModPKModifier = function(elementalModifier){
	var CombatSystem__ElementalModPKModifier = 0.25;
	return (elementalModifier - 1.0) * CombatSystem__ElementalModPKModifier + 1.0;
}

ItemExamineUI.prototype.InqPluralizedGemName = function(gem_type){
	if(gem_type == 38)
		return "Rubies";

	var gemname = this.InqMaterialName(gem_type);

	if ( gem_type == 11 
		|| gem_type == 24
		|| gem_type == 27
		|| gem_type == 29
		|| gem_type == 32
		|| gem_type == 37
		|| gem_type == 40
		|| gem_type == 46
		|| gem_type == 36
		|| gem_type == 45 ){
			var rhs = "pieces of ";
			return rhs + gemname;
	 }
	if(gem_type == 26 || gem_type == 49)
		return gemname + 'es';

	if(gem_type != 28)
		return gemname + 's';

	return gemname;
}
