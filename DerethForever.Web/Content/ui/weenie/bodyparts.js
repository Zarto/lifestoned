﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-body-part-armor', {
    props: ['armor', 'floats'],
    model: { prop: 'armor', event: 'changed' },
    methods: {
        calcArmor(v) {
            let q = this.floats.find((x) => x.key === v);
            let r = q ? q.value : 0;
            return Math.floor(r * this.armor.base_armor);
        }
    },
    watch: {
        'armor.base_armor': function (v, o) {
            this.armor.armor_vs_bludgeon = this.calcArmor(15);
            this.armor.armor_vs_pierce = this.calcArmor(14);
            this.armor.armor_vs_slash = this.calcArmor(13);
            this.armor.armor_vs_acid = this.calcArmor(18);
            this.armor.armor_vs_cold = this.calcArmor(16);
            this.armor.armor_vs_fire = this.calcArmor(17);
            this.armor.armor_vs_electric = this.calcArmor(19);
            this.armor.armor_vs_nether = this.calcArmor(165);
        }
    },
    template: `
    <div>
        <div class="row row-spacer">
            <div class="col-md-2">Base</div>
            <div class="col-md-2">Blunt</div>
            <div class="col-md-2">Pierce</div>
            <div class="col-md-2">Slash</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2"><input v-model.number="armor.base_armor" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="armor.armor_vs_bludgeon" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.armor_vs_pierce" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.armor_vs_slash" type="text" class="form-control" readonly /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2">Acid</div>
            <div class="col-md-2">Cold</div>
            <div class="col-md-2">Fire</div>
            <div class="col-md-2">Lightning</div>
            <div class="col-md-2">Nether</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2"><input v-model.number="armor.armor_vs_acid" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.armor_vs_cold" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.armor_vs_fire" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.armor_vs_electric" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.armor_vs_nether" type="text" class="form-control" readonly /></div>

        </div>

    </div>
    `
});

Vue.component('lsd-body-part-target', {
    props: ['part'],
    model: { prop: 'part', event: 'changed' },
    template: `
    <div>
        <hr />
        <div class="row row-spacer">
            <div class="col-md-2"></div>
            <div class="col-md-2">Front Left</div>
            <div class="col-md-2">Front Right</div>
            <div class="col-md-2">Back Left</div>
            <div class="col-md-2">Back Right</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2">High</div>
            <div class="col-md-2"><input v-model.number="part.HLF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.HRF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.HLB" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.HRB" type="text" class="form-control" /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2">Mid</div>
            <div class="col-md-2"><input v-model.number="part.MLF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.MRF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.MLB" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.MRB" type="text" class="form-control" /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2">Low</div>
            <div class="col-md-2"><input v-model.number="part.LLF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.LRF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.LLB" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.LRB" type="text" class="form-control" /></div>
        </div>
    </div>
    `
});

Vue.component('lsd-body-part', {
    props: ['kvp', 'floats'],
    model: { prop: 'kvp', event: 'changed' },
    computed: {
        part() {
            return this.kvp.value;
        }
    },
    methods: {
        deleted() {
            this.$emit('deleted', this.kvp);
        }
    },
    template: `
    <div>
        <div class="row row-spacer">
            <div class="col-md-3">Damage Type</div>
            <div class="col-md-2">Damage</div>
            <div class="col-md-2">Variance</div>
            <div class="col-md-2">Height</div>
            <div class="col-md-offset-2 col-md-1">
                <button @click="deleted" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-3"><lsd-enum-select type="DamageType" v-model="part.dtype" keyOnly></lsd-enum-select></div>
            <div class="col-md-2"><input v-model.number="part.dval" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.dvar" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.bh" type="text" class="form-control" /></div>

        </div>

        <lsd-body-part-armor v-model="part.acache" :floats="floats"></lsd-body-part-armor>
        <lsd-body-part-target v-model="part.bpsd"></lsd-body-part-target>
    </div>
    `
});

Vue.component('lsd-body-part-list', {
    props: ['body', 'floats'],
    model: { prop: 'body', event: 'changed' },
    data() {
        return {
            newPart: null
        };
    },
    computed: {
        parts() {
            return this.body ? this.body.body_part_table : [];
        }
    },
    methods: {
        openDialog() {
            this.$refs.modal.show();
        },
        openImport() {
            this.$refs.import.show();
        },
        addNew() {
            if (this.newPart) {
                if (!this.body) this.body = { body_part_table: [] };

                var key = parseInt(this.newPart.Key);
                this.body.body_part_table.push({ key: key, value: this.$weenie.newBodyPart() });
            }
        },
        importWeenie(weenie) {
            if (weenie && weenie.body) {
                this.$emit('changed', weenie.body);
            }
        },
        deleted(part) {
            var idx = this.body.body_part_table.indexOf(part);
            if (idx >= 0) {
                this.body.body_part_table.splice(idx, 1);
            }
        }
    },
    template: `
    <lsd-panel title="Body Parts" showAdd>

        <template v-slot:headerCommands>
        <div class="col-md-offset-4 col-md-2">
            <button @click="openImport" type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-import"></i></button>
            <button @click="openDialog" type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        </template>
        
        <ul class="nav nav-stacked col-md-2">
            <li v-for="(part, idx) in parts" :key="part.Key" :class="{ 'active' : (idx == 0) }"><a v-lsd-enum-display="{ type: 'BodyPartType', key: part.key }" :href="'#w_bp_'+idx" data-toggle="tab">{{ part.key }}</a></li>
        </ul>
        <div class="tab-content col-md-10">
            <div v-for="(part, idx) in parts" :id="'w_bp_'+idx" class="tab-pane" :class="{ 'active' : (idx == 0) }">
                <lsd-body-part v-model="parts[idx]" :floats="floats" @deleted="deleted"></lsd-body-part>
            </div>
        </div>

        <lsd-dialog ref="modal" title="Add Body Part" @saved="addNew">
            <lsd-enum-select type="BodyPartType" v-model="newPart"></lsd-enum-select>
        </lsd-dialog>

        <lsd-weenie-import-dialog ref="import" title="Import Body Part" @changed="importWeenie"></lsd-weenie-import-dialog>

    </lsd-panel>
    `
});

// bodypart     damage  variance    height  damagetype
// ---------------------------------------------------
// armor
// ---------------------------------------------------
// targeting
// ---------------------------------------------------