﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-weenie', {
    props: ['weenie'],
    model: { prop: 'weenie', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
        /*weenie: {
            get() { return this.$weenie.weenie; },
            set(val) { this.$weenie.weenie = val; }
        },*/
        itemType() {
            var item = this.weenie.intStats.find((v) => v.key === 1);
            return item ? item.value : 0;
        },
        iconId() {
            var item = this.weenie.didStats.find((v) => v.key === 8);
            return item ? item.value : 0;
        },
        uiEffects() {
            var item = this.weenie.intStats.find((v) => v.key === 18);
            return item ? item.value : 0;
        },
        iconUrl() {
            var iconUrl = `/Resource/GetDynamicIcon?itemType=${this.itemType}&iconId=${this.iconId}`;
            if (this.uiEffects > 0)
                iconUrl += `&uiEffects=${this.uiEffects}`;
            return iconUrl;
        },
        isNew() {
            return this.$weenie.isNew;
        }
    },
    created() {
    },
    methods: {

    },
    template: `
    <div>
        <div class="well">
            <div v-if="isNew">
                <div class="row row-spacer">
                    <div class="col-md-3">Weenie Name</div>
                    <div class="col-md-8"><input v-model="weenie.name" type="text" class="form-control" placeholder="Name" required /></div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3">Weenie Id</div>
                    <div class="col-md-8"><input v-model.number="weenie.wcid" type="number" class="form-control" placeholder="Weenie Id" required /></div>
                </div>
            </div>
            <div v-else class="row">
                <div class="col-md-1 weenie-icon">
                    <img v-if="iconId" :src="iconUrl" />
                </div>
                <div class="col-md-7">
                    <h2 class="well-title">{{ weenie.name }} ({{ weenie.wcid }})</h2>
                </div>
                <div v-if="weenie.modifiedBy" class="col-md-4">
                    <div class="strong text-right">Modified {{ weenie.lastModified | toDate }} | {{ weenie.modifiedBy }}</div>
                </div>
            </div>
            <div class="row row-spacer">
                <div class="col-md-3">Weenie Type</div>
                <div class="col-md-8">
                <lsd-enum-select type="WeenieType" v-model="weenie.weenieType" keyOnly required></lsd-enum-select>
                </div>
            </div>
        </div>

        <div class="tab-content">

        <div id="general" class="tab-pane fade in active">

            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Properties</h3>

                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="tab" href="#stringProperties">String<span class="badge">{{ weenie.stringStats ? weenie.stringStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#int32Properties">Int32<span class="badge">{{ weenie.intStats ? weenie.intStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#int64Properties">Int64<span class="badge">{{ weenie.int64Stats ? weenie.int64Stats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#dblProperties">Float<span class="badge">{{ weenie.floatStats ? weenie.floatStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#didProperties">Data ID<span class="badge">{{ weenie.didStats ? weenie.didStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#iidProperties">Instance ID<span class="badge">{{ weenie.iidStats ? weenie.iidStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#boolProperties">Bool<span class="badge">{{ weenie.boolStats ? weenie.boolStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#positions">Position<span class="badge">{{ weenie.posStats ? weenie.posStats.length : 0 }}</span></a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <lsd-string-props id="stringProperties" v-model="weenie.stringStats" :active="true"></lsd-string-props>

                    <lsd-int-props id="int32Properties" v-model="weenie.intStats"></lsd-int-props>

                    <lsd-long-props id="int64Properties" v-model="weenie.int64Stats"></lsd-long-props>

                    <lsd-double-props id="dblProperties" v-model="weenie.floatStats"></lsd-double-props>

                    <lsd-did-props id="didProperties" v-model="weenie.didStats"></lsd-did-props>

                    <lsd-iid-props id="iidProperties" v-model="weenie.iidStats"></lsd-iid-props>

                    <lsd-bool-props id="boolProperties" v-model="weenie.boolStats"></lsd-bool-props>

                    <lsd-positions id="positions" v-model="weenie.posStats"></lsd-positions>

                </div>
            </div>

            <lsd-item-spell-table v-model="weenie.spellbook"></lsd-item-spell-table>
        </div>

        <div id="creature" class="tab-pane fade">
            <div v-if="itemType==16">
            <lsd-panel title="Attributes">
                <lsd-attributes v-model="weenie.attributes"></lsd-attributes>
            </lsd-panel>

            <lsd-skills
                v-model="weenie.skills"
                :attributes="weenie.attributes"
                @removed="weenie.skills.splice($event, 1)">
            </lsd-skills>
            
            <lsd-body-part-list v-model="weenie.body" :floats="weenie.floatStats"></lsd-body-part-list>
            </div>
            <div v-else>Set Item Type to Creature</div>
        </div>

        <div id="book" class="tab-pane fade">
            <lsd-book v-if="weenie.weenieType==8" v-model="weenie.pageDataList"></lsd-book>
            <div v-else>Set Weenie Type to Book to manage pages</div>
        </div>

        <div id="createlist" class="tab-pane fade">
            <lsd-create-items v-model="weenie.createList"></lsd-create-items>
        </div>

        <div id="generator" class="tab-pane fade">
            <lsd-generator v-model="weenie.generatorTable"></lsd-generator>
        </div>

        <div id="emote" class="tab-pane fade">
            <lsd-emote-table v-model="weenie.emoteTable"></lsd-emote-table>
        </div>

        <div id="history" class="tab-pane fade">
            <lsd-weenie-history v-model="weenie"></lsd-weenie-history>
        </div>
		
		<div id="appraise" class="modal" tabindex="-1" role="dialog">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h2 class="modal-title"></h2>
			  </div>
			  <div class="modal-body"></div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>

        </div>

    </div>


    `
});

Vue.component('lsd-weenie-history', {
    props: ['weenie'],
    model: { prop: 'weenie', event: 'changed' },
    data() {
        return {};
    },
    methods: {
    },
    template: `
    <lsd-panel title="Change Log" stickHeader>
        <div class="row row-spacer">
            <div class="col-md-2">Changelog Entry</div>
            <div class="col-md-8"><textarea v-model="weenie.userChangeSummary" class="form-control" rows="6" required></textarea></div>
        </div>

        <hr />

        <div v-for="entry in weenie.changelog" class="row row-spacer">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ entry.author }} on {{ entry.created | toDate }}
                    </div>
                    <div class="panel-body text-prewrap">{{ entry.comment }}</div>
                </div>
            </div>
        </div>

    </lsd-panel>
    `
});

Vue.component('lsd-weenie-import-dialog', {
    props: [ 'title' ],
    data() {
        return {
            importedWeenie: null
        };
    },
    methods: {
        show() {
            this.$refs.modal.show();
        },
        importWeenie() {
            if (this.importedWeenie) {
                var key = parseInt(this.importedWeenie);
                if (!key) return;
                // TODO: make this a service method
                var $this = this;
                $.getJSON("/Weenie/Get", { id: key },
                    function (res) {
                        if (res) {
                            $this.$emit('changed', res);
                        }
                    });
            }
        }
    },
    template: `
    <lsd-dialog ref="modal" :title="title" @saved="importWeenie">
        <input v-lsd-weenie-finder v-model="importedWeenie" type="text" class="form-control" placeholder="Weenie" />
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    </lsd-dialog>
    `
});
