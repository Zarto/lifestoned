﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lifestoned.DataModel.Account;
using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Gdle.Quests;
using Lifestoned.DataModel.Gdle.Recipes;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.DataModel.Gdle.Spells;
using Lifestoned.DataModel.Shared;
using Lifestoned.DataModel.WorldRelease;
using Lifestoned.Providers;
using Newtonsoft.Json;

namespace DerethForever.Web.Controllers
{
    [Authorize]
    public class SandboxController : BaseController
    {
        // GET: Sandbox
        public ActionResult Index()
        {
            SandboxModel model = new SandboxModel();
            Guid uid = new Guid(GetUserGuid());
            model.Entries = SandboxContentProviderHost.GetChangeEntries(uid).ToList();
            ////model.Entries = SandboxContentProviderHost.CurrentProvider.GetChanges(new Guid(GetUserGuid())).ToList();
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Delete(uint id, string type)
        {
            Guid uid = new Guid(GetUserGuid());
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                SandboxContentProviderHost.DeleteChange(uid, entry);
                ////SandboxContentProviderHost.CurrentProvider.DeleteChange(uid, entry);
            }

            return Json(true);
        }

        [HttpGet]
        [Authorize(Roles = "Envoy")]
        public ActionResult Submissions()
        {
            SandboxModel model = new SandboxModel();
            BaseModel current = CurrentBaseModel;
            BaseModel.CopyBaseData(current, model);
            CurrentBaseModel = current;

            IEnumerable<ChangeEntry> entries = null;

            if (User.IsInRole("Envoy"))
                entries = SandboxContentProviderHost.GetChangeEntries();
            ////entries = SandboxContentProviderHost.CurrentProvider.GetChanges();
            else
                entries = SandboxContentProviderHost.GetChangeEntries(new Guid(GetUserGuid()));
            ////entries = SandboxContentProviderHost.CurrentProvider.GetChanges(new Guid(GetUserGuid()));

            model.Entries = entries.Where(x => x != null && x.Submitted);//.ToList();

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddDiscussionComment(string userGuid, uint itemId, string type, string discussionComment, string source)
        {
            string currentUser = GetUserGuid();

            if (!(User.IsInRole("Developer") || userGuid == currentUser))
            {
                // only the submitter and developers can comment
                return RedirectToAction(source);
            }

            Guid uid = new Guid(userGuid);

            ChangeEntry change = SandboxContentProviderHost.GetChangeEntry(type, uid, itemId);
            ////IEnumerable<ChangeEntry> temp = SandboxContentProviderHost.CurrentProvider.GetChanges(new Guid(userGuid), type);
            ////ChangeEntry change = temp.FirstOrDefault(x => x.UserGuid == userGuid && x.EntryId == itemId);

            if (change == null)
                return RedirectToAction(source);

            change.Discussion.Add(new ChangeDiscussionEntry()
            {
                Comment = discussionComment,
                Created = DateTime.Now,
                UserName = GetUserName(),
                UserGuid = Guid.Parse(GetUserGuid())
            });

            ////SandboxContentProviderHost.CurrentProvider.UpdateChange(new Guid(userGuid), change);
            SandboxContentProviderHost.UpdateChange(uid, change);

            return RedirectToAction(source);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Submit(uint id, string type)
        {
            Guid uid = new Guid(GetUserGuid());
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                entry.Submitted = true;
                entry.SubmissionTime = DateTime.Now;
                ////SandboxContentProviderHost.CurrentProvider.UpdateChange(uid, entry);
                SandboxContentProviderHost.UpdateChange(uid, entry);

                DiscordController.PostChangeAsync(entry);
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Withdraw(uint id, string type)
        {
            Guid uid = new Guid(GetUserGuid());
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                entry.Submitted = false;
                ////SandboxContentProviderHost.CurrentProvider.UpdateChange(uid, entry);
                SandboxContentProviderHost.UpdateChange(uid, entry);
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize(Roles = "Developer")]
        public ActionResult Accept(uint id, string type, string userId, bool done)
        {
            Guid uid = new Guid(userId);
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                if (!string.IsNullOrEmpty(entry.ChangeLog.UserChangeSummary))
                {
                    entry.ChangeLog.Changelog.Add(new ChangelogEntry()
                    {
                        Author = entry.UserName,
                        Created = entry.SubmissionTime,
                        Comment = entry.ChangeLog.UserChangeSummary
                    });
                }

                if (done)
                    entry.Metadata.IsDone = true;

                // copy to final
                ////SandboxContentProviderHost.CurrentProvider.AcceptChange(uid, entry);
                SandboxContentProviderHost.AcceptChange(uid, entry);

                // delete change
                ////SandboxContentProviderHost.CurrentProvider.DeleteChange(uid, entry);
                SandboxContentProviderHost.DeleteChange(uid, entry);

                // submit to discord
                DiscordController.PostAcceptAsync(entry, CurrentUser.DisplayName);
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize(Roles = "Developer")]
        public ActionResult Reject(uint id, string type, string userId, string comment)
        {
            Guid uid = new Guid(userId);
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                entry.Submitted = false;
                entry.Discussion.Add(new ChangeDiscussionEntry()
                {
                    Comment = comment,
                    Created = DateTime.Now,
                    UserGuid = new Guid(GetUserGuid()),
                    UserName = GetUserName()
                });

                ////SandboxContentProviderHost.CurrentProvider.UpdateChange(uid, entry);
                SandboxContentProviderHost.UpdateChange(uid, entry);
            }

            return new EmptyResult();
        }

        [HttpGet]
        [Authorize]
        public ActionResult Download(bool sandbox = false)
        {
            if (!sandbox && !User.IsInRole("Envoy"))
                return UnauthorizedResult();

            Guid uid = new Guid(GetUserGuid());

            IEnumerable<ChangeEntry> entries = null;
            if (!sandbox)
                entries = SandboxContentProviderHost.GetChangeEntries().Where(x => x.Submitted);
            else
                entries = SandboxContentProviderHost.GetChangeEntries(uid);

            var archive = new ZipFile();
            var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

            SortedDictionary<uint, SpellTableEntry> tempSpells = new SortedDictionary<uint, SpellTableEntry>();
            SortedDictionary<string, QuestTableEntry> tempQuests = new SortedDictionary<string, QuestTableEntry>();

            foreach (var update in entries)
            {
                string content = JsonConvert.SerializeObject(update.ChangeLog, settings);

                switch (update.EntryType)
                {
                    case WeenieChange.TypeName:
                        Weenie w = update.ChangeLog as Weenie;
                        archive.AddFile($"weenies/{FormatFileName(w.WeenieClassId, w.Name)}", content);
                        break;

                    case SpawnMapChange.TypeName:
                        SpawnMapEntry s = update.ChangeLog as SpawnMapEntry;
                        archive.AddFile($"spawnMaps/{FormatFileName(s.Key, s.Description)}", content);
                        break;

                    case RecipeChange.TypeName:
                        Recipe r = update.ChangeLog as Recipe;
                        archive.AddFile($"recipes/{FormatFileName(r.Key, r.Description)}", content);
                        break;

                    case SpellTableChange.TypeName:
                        SpellTableEntry spell = update.ChangeLog as SpellTableEntry;
                        tempSpells.Add(spell.Key, spell);
                        break;
                }
            }

            {
                // spells have to be all of them
                // so get all the base spells and fill out the structure, and add updates
                // from the dictionary above
                SpellsFile spellTable = new SpellsFile();
                var spells = ContentProviderHost.GetContent(SpellTableChange.TypeName).Cast<SpellTableEntry>();
                foreach (SpellTableEntry spell in spells)
                {
                    if (tempSpells.TryGetValue(spell.Key, out SpellTableEntry tempSpell))
                        spellTable.Table.Entries.Add(tempSpell);
                    else
                        spellTable.Table.Entries.Add(spell);
                }

                string content = JsonConvert.SerializeObject(spellTable, settings);
                archive.AddFile("spells.json", content);
            }

            {
                // same for quests
                List<QuestTableEntry> questTable = new List<QuestTableEntry>();
                var quests = ContentProviderHost.GetContent(QuestChange.TypeName).Cast<QuestTableEntry>();
                foreach (QuestTableEntry quest in quests)
                {
                    if (tempQuests.TryGetValue(quest.Key, out QuestTableEntry q))
                        questTable.Add(q);
                    else
                        questTable.Add(quest);
                }

                string content = JsonConvert.SerializeObject(questTable, settings);
                archive.AddFile("quests.json", content);
            }

            System.IO.Stream stream = archive.BuildZipStream();
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            return File(stream, "application/zip", WorldReleaseController.GetDownloadFileName(sandbox ? ReleaseType.Sandbox : ReleaseType.Test));
        }
    }
}