﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using log4net;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Lifestoned.DataModel
{
    public class EnsureUnsignedJsonConverter : JsonConverter
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(int) || objectType == typeof(uint);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
			if (reader.TokenType == JsonToken.Integer)
            {
				if (reader.ValueType == typeof(long))
                {
                    return unchecked((uint)(long)reader.Value);
                }

				if (reader.ValueType == typeof(int))
                {
                    return unchecked((uint)reader.Value);
                }

				if (reader.ValueType == typeof(uint))
                {
                    return (uint)reader.Value;
                }
            }

            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value != null)
            {
                writer.WriteValue(value);
                return;
            }

            writer.WriteNull();
        }
    }
}
