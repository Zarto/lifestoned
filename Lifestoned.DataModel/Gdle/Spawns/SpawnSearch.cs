﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.DataModel.Gdle.Spawns
{
    public class SpawnSearch : BaseModel
    {
		public string Criteria { get; set; }

		public class Result
        {
			public SpawnMapEntry Entry { get; set; }
        }
    }
}
