﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lifestoned.DataModel.Shared
{
    public interface IMetadata : IChangeLog
    {
        DateTime? LastModified { get; set; }
        string ModifiedBy { get; set; }
	    bool IsDone { get; set; }
    }
}
