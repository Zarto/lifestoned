﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Lifestoned.DataModel.Content;
using Lifestoned.DataModel.Shared;

namespace Lifestoned.Providers.Database
{
    public class DatabasePatchContentProvider : SQLiteDatabaseProvider, IPatchContentProvider
    {
		public DatabasePatchContentProvider() : base("PatchDbConnection")
        {
        }

        protected override void OnInitialize(DbConnection connection)
        {
            connection.Execute($@"
				CREATE TABLE IF NOT EXISTS Patch (
				Id INT PRIMARY KEY,
				LastModified DATETIME,
				UserModified VARCHAR(100) COLLATE NOCASE,
				Name VARCHAR(250) COLLATE NOCASE,
				ReleaseDate DATETIME,
				Complete INT DEFAULT 0 NOT NULL,
				CompleteDate DATETIME);");

            connection.Execute($@"
				CREATE TABLE IF NOT EXISTS PatchContent (
				PatchId INT NOT NULL,
				ContentType INT NOT NULL,
				ContentId INT NOT NULL,

				LastModified DATETIME,
				UserModified VARCHAR(100) COLLATE NOCASE,
				Name VARCHAR(250) COLLATE NOCASE,
				UserAssigned BINARY(16),
				Complete INT DEFAULT 0 NOT NULL,
				CompleteDate DATETIME,
				PRIMARY KEY (PatchId, ContentType, ContentId),
				FOREIGN KEY (PatchId) REFERENCES Patch(Id) );");
        }

        public IEnumerable<Patch> GetPatch()
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    return db.Query<Patch>($"SELECT * FROM Patch");
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public Patch GetPatch(int id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    return db.QuerySingle<Patch>($"SELECT * FROM Patch WHERE id=@id", new { id });
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public bool SavePatch(Patch item)
        {
            return UpdatePatch(item);
        }

        public bool UpdatePatch(Patch item)
        {
            item.LastModified = DateTime.UtcNow;
            try
            {
                using (DbConnection db = GetConnection())
                {
                    db.Execute($@"
						INSERT INTO Patch (Id, LastModified, UserModified, Name, ReleaseDate, Complete, CompleteDate)
						VALUES (@Id, @LastModified, @UserModified, @Name, @ReleaseDate, @Complete, @CompleteDate)
						ON CONFLICT (Id) DO UPDATE SET
						LastModified=@LastModified, UserModified=@ModifiedBy, Name=@Name,
						ReleaseDate=@ReleaseDate, Complete=@Complete, CompleteDate=@CompleteDate
						WHERE Id=@Id;
						", item);
                    return true;
                }
            }
            catch (DbException ex)
            {
                return false;
            }
        }

        public bool DeletePatch(int id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                    db.Execute($"DELETE FROM Patch WHERE id=@id", new { id });
                return true;
            }
            catch (DbException)
            {
                return false;
            }
        }

        public IEnumerable<PatchContent> GetPatchContent(int patchId)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    return db.Query<PatchContent>($"SELECT * FROM PatchContent WHERE patchId=@patchId", new { patchId });
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public IEnumerable<PatchContent> GetPatchContent(int patchId, ContentType type)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    return db.Query<PatchContent>(
						$"SELECT * FROM PatchContent WHERE patchId=@patchId AND contentType=@type",
						new { patchId, type });
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public PatchContent GetPatchContent(int patchId, ContentType type, int id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    return db.QuerySingle<PatchContent>(
                        $"SELECT * FROM PatchContent WHERE patchId=@patchId AND contentType=@type AND contentId=@id",
                        new { patchId, type, id });
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public bool SavePatchContent(PatchContent item)
        {
            return UpdatePatchContent(item);
        }

        public bool UpdatePatchContent(PatchContent item)
        {
            item.LastModified = DateTime.UtcNow;
            try
            {
                using (DbConnection db = GetConnection())
                {
                    db.Execute($@"
						INSERT INTO PatchContent (PatchId, ContentType, ContentId, LastModified, UserModified, Name, UserAssigned, Complete, CompleteDate)
						VALUES (@PatchId, @ContentType, @ContentId, @LastModified, @UserModified, @Name, @UserAssigned, @Complete, @CompleteDate)
						ON CONFLICT (PatchId, ContentType, ContentId) DO UPDATE SET
						LastModified=@LastModified, UserModified=@UserModified, Name=@Name,
						UserAssigned=@UserAssigned, Complete=@Complete, CompleteDate=@CompleteDate
						WHERE PatchId=@PatchId AND ContentType=@ContentType AND ContentId=@ContentId;
						", item);
                    return true;
                }
            }
            catch (DbException ex)
            {
                return false;
            }
        }

        public bool DeletePatchContent(int patchId, ContentType type, int id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                    db.Execute(
						$"DELETE FROM PatchContent WHERE patchId=@patchId AND contentType=@type AND contentId=@id",
						new { patchId, type, id });
                return true;
            }
            catch (DbException)
            {
                return false;
            }
        }

        public Patch GetContentPatch(ContentType type, int id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    return db.QuerySingle<Patch>(
						$"SELECT p.* FROM Patch p INNER JOIN PatchContent pc ON p.Id = pc.PatchId WHERE pc.ContentType=@type AND pc.ContentId=@id",
						new { type, id });
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
