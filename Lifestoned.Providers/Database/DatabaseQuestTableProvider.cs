﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle.Quests;

namespace Lifestoned.Providers.Database
{
    public class DatabaseQuestTableProvider : SQLiteContentDatabase<QuestTableEntry>, IQuestTableProvider
    {
        public DatabaseQuestTableProvider() : base("QuestTableDbConnection", "Quests", (o) => o.Id.Value)
        {
        }
    }

    public class DatabaseQuestTableSandboxProvider : SQLiteSandboxDatabase<QuestTableEntry, QuestChange>, IQuestTableSandboxProvider
    {
        public DatabaseQuestTableSandboxProvider() : base("QuestTableDbConnection", "Quests_Sandbox", (o) => o.Id.Value)
        {
        }
    }
}
