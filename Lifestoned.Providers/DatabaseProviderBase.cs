﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace Lifestoned.Providers
{
    public class DatabaseProviderBase
    {
        protected DbProviderFactory DbFactory { get; private set; }

        protected string ConnectionString { get; private set; }

        private bool initialized;

        protected DatabaseProviderBase(string connectionName)
        {
            ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings[connectionName];
            if (cs == null)
                throw new InvalidOperationException($"{connectionName} connection string not configured");

            ConnectionString = cs.ConnectionString;
            DbFactory = DbProviderFactories.GetFactory(cs.ProviderName);
        }

        protected DbConnection GetConnection()
        {
            DbConnection connection = DbFactory.CreateConnection();
            connection.ConnectionString = ConnectionString;
            connection.Open();

            if (!initialized)
            {
                OnInitialize(connection);
                initialized = true;
            }

            OnSetupConnection(connection);
            return connection;
        }

		protected virtual void OnInitialize(DbConnection connection)
        {
        }

		protected virtual void OnSetupConnection(DbConnection connection)
        {
        }
    }
}