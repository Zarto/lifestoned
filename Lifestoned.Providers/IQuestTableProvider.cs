﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lifestoned.DataModel.Gdle.Quests;

namespace Lifestoned.Providers
{
    public interface IQuestTableProvider : IGenericContentProvider<QuestTableEntry>
    {
    }

    public interface IQuestTableSandboxProvider : IQuestTableProvider, IGenericSandboxContentProvider<QuestTableEntry>
    {
    }
}
