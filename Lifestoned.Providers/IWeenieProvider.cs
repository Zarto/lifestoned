﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lifestoned.DataModel.Gdle;

namespace Lifestoned.Providers
{
    public interface IWeenieProvider : IGenericContentProvider<Weenie>
    {
    }

    public interface IWeenieSandboxProvider : IWeenieProvider, IGenericSandboxContentProvider<Weenie>
    {
    }
}
